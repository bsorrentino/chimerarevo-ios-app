//
//  NSString+MBRequest.h
//  MBRequest
//
//  Created by Sebastian Celis on 3/1/12.
//  Copyright (c) 2012 Mobiata, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MBRequest)

- (NSString *)mb_URLEncodedString;

@end
