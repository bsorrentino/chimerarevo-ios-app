//
//  BSCPostData.m
//  ChimeraRevoApp
//
//  Created by softphone on 30/03/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

const NSString * BSCLinkNextPageRel = @"next_page";
const NSString * BSCLinkPrevPageRel = @"prev_page";

#import "BSCPostData.h"

@interface BSCPostData ()

@property (nonatomic,strong)  NSDictionary *jsonData;

- (NSString *)getLinkHref:(const NSString *)rel;

@end

@implementation BSCPostData

-(instancetype)initFormJsonData:(id)jsonData
{
    self = [self init];
    if( self ) {
        
        _jsonData = jsonData;
    }
    return self;
}

- (NSArray *)getPosts
{
    if( !_jsonData ) return  @[];
    
    return [_jsonData valueForKey:@"posts"] ;
    
}

- (NSInteger)getTotalPages
{
    if( !_jsonData ) return 0;
    
    NSNumber *v = [_jsonData valueForKey:@"totalPages"] ;
    
    return [v intValue] ;
    
}
- (NSInteger)getPage
{
    if( !_jsonData ) return 0;
    
    NSNumber *v = [_jsonData valueForKey:@"page"] ;
    
    return [v intValue] ;
    
}

- (NSString *)getLinkHref:(const NSString *)name
{
    if( !_jsonData ) return nil;
    
    NSString * result = nil;
    NSArray * links = [_jsonData valueForKey:@"links"];
    
    for (NSDictionary *link in links ) {
        
        NSString * rel = [link valueForKey:@"rel"];
        
        if ([name compare:rel options:NSCaseInsensitiveSearch]==0) {
            
            result = [link valueForKey:@"href"];
            
            break;
        }
    }
    
    return result;
    
}

- (NSURL *)getPrevPage
{
    NSString *href = [self getLinkHref:BSCLinkPrevPageRel];
    
    return (href) ? [NSURL URLWithString:href] : nil;
}

- (NSURL *)getNextPage
{
    NSString *href = [self getLinkHref:BSCLinkNextPageRel];
    
    return (href) ? [NSURL URLWithString:href] : nil;
    
}

- (NSArray *)selectLatest:(id)post
{
    if (!post) {
        return self.posts;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:FORMAT_TIMEZONE_ABBREV]];
    [dateFormatter setDateFormat:FORMAT_DATETIME];
    
    
    NSDate *date = [dateFormatter dateFromString:[post valueForKey:@"data"]];
    
    NSInteger index = [self.posts count];
    
    for( NSInteger i = index-1 ; i >=0 ; --i ) {
        
        id p = self.posts[i];
        
        NSDate *dt = [dateFormatter dateFromString:[p valueForKey:@"data"]];
        
        if( [dt compare:date] == NSOrderedDescending ) {
            
            return [self.posts subarrayWithRange:NSMakeRange(0, i+1)];
        }
    }
    
    return @[];
}



@end
