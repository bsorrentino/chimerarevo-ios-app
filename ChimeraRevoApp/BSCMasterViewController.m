//
//  BSCMasterViewController.m
//  ChimeraRevoApp
//
//  Created by softphone on 29/01/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <MTBlockTableView/MTBlockTableView.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SVPullToRefresh/SVPullToRefresh.h>
#import <SASlideMenu/SASlideMenuViewController.h>
#import <UISearchBar-Blocks/UISearchBar+Blocks.h>
#import <LARSAdController/LARSAdController.h>

#import "BSCDetailViewController.h"
#import "BSCDetailRootViewController.h"
#import "BSCMasterViewController.h"

#import "BSCServices.h"

#import "BSCPostData.h"
#import "BSCLoadPostViewController.h"

#import "BSCNavigationController.h"

@interface BSCMasterViewController () {
    
    BOOL _typeIsChanged;
    
}

@property (nonatomic,strong)                              NSDictionary *jsonDataSearch;
@property (nonatomic,strong)                              NSDictionary *jsonData;

@property (nonatomic,strong,getter=getMainDataSource)     NSMutableArray *mainDS;
@property (nonatomic,strong,getter=getFilterDataSource)   NSMutableArray *filterDS;
@property (nonatomic,strong,getter=getSearchDataSource)   NSMutableArray *searchDS;


@property (nonatomic,weak,readonly,getter=getDataSource)  NSMutableArray  *dataSource;
@property (nonatomic,strong)                              NSString        *category;
@property (nonatomic,strong)                              NSString *type;


@property (nonatomic,weak,getter=getPosts,readonly)       NSArray *posts;
@property (nonatomic,getter=getTotalPages,readonly)       NSInteger totalPages;
@property (nonatomic,getter=getPage,readonly)             NSInteger page;
@property (nonatomic,weak,getter=getNextPage,readonly)    NSURL *nextPage;
@property (nonatomic,weak,getter=getPrevPage,readonly)    NSURL *prevPage;

@property (nonatomic,strong, readonly)                    BSCLoadPostViewController *loadPostsVC;

@property ()   BOOL isSearchActive;

-(void)pullToRefrehDidLoad;
-(void)navigationItemDidLoad;

- (NSString *)getLinkHref:(const NSString *)rel;

- (void)loadNextPage;
- (void)insertPostsAtBottom;
- (NSInteger)insertPostsAtTop:(BSCPostData *)data;

-(void)searchAction:(id)sender;
-(void)rightMenuAction;

- (BOOL)isFilterActive;
- (BOOL)isSearchActive;
@end

@implementation BSCMasterViewController

@synthesize loadPostsVC=_loadPostsVC;

-(NSString *)type
{
    return (_type) ? _type : TYPE_POSTS;
}

-(BSCLoadPostViewController *)loadPostsVC
{
    if( !_loadPostsVC ) {
        
        _loadPostsVC = [[BSCLoadPostViewController alloc] init];
    }
    
    return _loadPostsVC;
}

- (void)loadNextPage
{
    __BLOCKSELF;

    MBJSONRequestCompletionHandler handler = ^(id responseJSON, NSError *error) {
        
        if (error == nil ) {
            __self.jsonData = responseJSON;
            [__self insertPostsAtBottom];
        }
        else {
            NSLog(@"error %@", error);
            __self.jsonData = nil;
        }
        
        [__self.tableView.infiniteScrollingView stopAnimating];
        
    };
    
    NSURL *nextPageUrl = self.nextPage;
    
    if (nextPageUrl) {
        
            [[BSCServices sharedInstance] loadPostsByUrl:nextPageUrl
                                             handler:handler];
    }
    else if( self.page == 0 ) {
    
        if (self.isSearchActive) {
            [[BSCServices sharedInstance] searchPostsByPage:1
                                                  theQuery:self.searchBar.text
                                              theCategory:self.category
                                               theHandler:handler];
        }
        else {
            [[BSCServices sharedInstance] loadPostsByPage:1
                                                  theType:self.type
                                              theCategory:self.category
                                               theHandler:handler];
        }
    }
    else {
        [self.tableView.infiniteScrollingView stopAnimating];
    }
}

- (NSInteger)insertPostsAtTop:(BSCPostData *)data {

    id lastPost = ( [self.dataSource count] > 0 ) ? self.dataSource[0] : nil;
    
    NSArray *news =  [data selectLatest:lastPost];
    
    NSInteger count = (news) ? [news count] : 0;
    
    NSLog(@"NEWS [%ld]", (long)count );
    
    if (count > 0) {
        
        [self.dataSource insertObjects:news
                             atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, count)]];
        if( ![self isFilterActive] )
            postRefreshNews(self.dataSource);

        @autoreleasepool {
            [self.tableView beginUpdates];
            
            NSMutableArray *indexes = [[NSMutableArray alloc] initWithCapacity:count];
            
            for (int i=0 ; i < count; ++i ) {
                
                [indexes addObject:[NSIndexPath indexPathForRow:i
                                                      inSection:0]];
                
            }
            [self.tableView insertRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationBottom];
            
            [self.tableView endUpdates];
        }

    }

    return count;
}

- (void)insertPostsAtBottom {
    
    //assert([self.posts count]>0);
    if ([self.posts count] <= 0 ) {
        return;
    }
    
    if( [self.dataSource count] == 0 ) {
        
        [self.dataSource addObjectsFromArray:self.posts];
        
        if( ![self isFilterActive] && !self.isSearchActive )
            postRefreshNews(self.dataSource);
        
        [self.tableView reloadData];
        
        return;
    }
    
    
    unsigned long startIndex = self.dataSource.count-1 ;

    [self.dataSource addObjectsFromArray:self.posts];
    if( ![self isFilterActive] )
        postRefreshNews(self.dataSource);
    
@autoreleasepool {
    [self.tableView beginUpdates];

    NSMutableArray *indexes = [[NSMutableArray alloc] initWithCapacity:[self.posts count]];
    
    for (int i=0 ; i < [self.posts count]; ++i ) {
        
        NSIndexPath *ii = [NSIndexPath indexPathForRow:startIndex+i
                                             inSection:0];
        [indexes addObject:ii];
        
    }
    [self.tableView insertRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationBottom];
    
    [self.tableView endUpdates];
}

/*
    __BLOCKSELF;
    
    int64_t delayInSeconds = 2.0;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [__self.tableView beginUpdates];
        [__self.dataSource addObject:[__self.dataSource.lastObject dateByAddingTimeInterval:-90]];
        [__self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:__self.dataSource.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        [__self.tableView endUpdates];
        
        [__self.tableView.infiniteScrollingView stopAnimating];
    });
*/
}

-(BOOL)isFilterActive
{
    return (self.category!=nil) || _typeIsChanged;
}

-(NSMutableArray *)getMainDataSource
{
    if (!_mainDS) {
        _mainDS = [[NSMutableArray alloc] init];
    }
    
    return _mainDS;
}
-(NSMutableArray *)getFilterDataSource
{
    if (!_filterDS) {
        _filterDS = [[NSMutableArray alloc] init];
    }
    
    return _filterDS;
}

-(NSMutableArray *)getSearchDataSource
{
    if (!_searchDS) {
        _searchDS = [[NSMutableArray alloc] init];
    }
    
    return _searchDS;
}

-(NSMutableArray *)getDataSource
{
    if ([self isSearchActive]) {
        return self.searchDS;
    }
    
    return (self.isFilterActive) ? self.filterDS : self.mainDS ;
    
}


- (NSArray *)getPosts
{
    if( !_jsonData ) return  @[];
    
    return [_jsonData valueForKey:@"posts"] ;

}


- (NSInteger)getTotalPages
{
    if( !_jsonData ) return 0;
    
    NSNumber *v = [_jsonData valueForKey:@"totalPages"] ;
    
    return [v intValue] ;
    
}
- (NSInteger)getPage
{
    if( !_jsonData ) return 0;
    
    NSNumber *v = [_jsonData valueForKey:@"page"] ;
    
    return [v intValue] ;
    
}

- (NSString *)getLinkHref:(const NSString *)name
{
    if( !_jsonData ) return nil;
    
    NSString * result = nil;
    NSArray * links = [_jsonData valueForKey:@"links"];
    
    for (NSDictionary *link in links ) {
        
        NSString * rel = [link valueForKey:@"rel"];
        
        if ([name compare:rel options:NSCaseInsensitiveSearch]==0) {
            
            result = [link valueForKey:@"href"];
            
            break;
        }
    }
    
    return result;
    
}

- (NSURL *)getPrevPage
{
    NSString *href = [self getLinkHref:BSCLinkPrevPageRel];
    
    return (href) ? [NSURL URLWithString:href] : nil;
}

- (NSURL *)getNextPage
{
    NSString *href = [self getLinkHref:BSCLinkNextPageRel];
    
    return (href) ? [NSURL URLWithString:href] : nil;
    
}

-(void)rightMenuAction
{
}


-(void)pullToRefrehDidLoad
{
    __BLOCKSELF;
    

    [self.tableView.infiniteScrollingView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        
        //if ([LARSAdController sharedManager].adVisible ) {
        //}
        
        NSLog(@"addInfiniteScrollingWithActionHandler");
        // append data to data source, insert new cells at the end of table view
        
        [__self loadNextPage];
        
    }];
    
    [self.tableView triggerInfiniteScrolling];
}

- (void)navigationItemDidLoad
{
    
    UIBarButtonItem *searchButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
                                                      target:self
                                                      action:@selector(searchAction:)];
    
#if 0 // SET SIMPLE BUTTON
    UIButton* rightMenuButton = [[UIButton alloc] init];
    rightMenuButton.frame = CGRectMake(0, 0, 21, 29);
    
    //[rightMenuButton setImage:[UIImage imageNamed:@"menuicon"] forState:UIControlStateNormal];
    
    [rightMenuButton setTitle:@"News" forState:UIControlStateNormal];
    [rightMenuButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    rightMenuButton.titleLabel.font = [rightMenuButton.titleLabel.font fontWithSize:8];
   

#else // SET BUTTON WITH WEBVIEW
    
    self.loadPostsVC.view.frame = CGRectMake(0, 0, 21, 29);
    self.loadPostsVC.webView.userInteractionEnabled = NO;
    UIButton *rightMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightMenuButton addSubview:self.loadPostsVC.webView];
    [rightMenuButton setFrame:CGRectMake(0, 0, 21, 29)];
#endif

    [rightMenuButton addTarget:self.presentedViewController
                        action:@selector(rightMenuAction)
              forControlEvents:UIControlEventTouchDown];

    
    __block UIBarButtonItem *newsButton = [[UIBarButtonItem alloc] initWithCustomView:rightMenuButton];
    

    [self.navigationItem setRightBarButtonItems:@[ newsButton, searchButton ]];
    
}

#pragma mark - Search

//#define MAGIC_SEARCH_HEIGHT 20
-(void)resetSearchState
{
    self.searchBar.text = @"";
    
    self.isSearchActive = NO;
    
    if (self.jsonDataSearch ) {
        self.jsonData = self.jsonDataSearch;
        self.jsonDataSearch = nil;
    }
    
}


-(void)searchAction:(id)sender
{
    
    if ([self.dataSource count] == 0 ) {
        return;
    }
    
    __BLOCKSELF;

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:NO];
    
    [UIView animateWithDuration:0.3 animations:^(void){
        __self.tableView.contentOffset =
        CGPointMake(0, (__self.tableView.bounds.origin.y - __self.searchBar.frame.size.height));

        
    } completion:^(BOOL finished){
        
        if( finished) [__self.searchBar becomeFirstResponder];
    }];
    
    
    
}



-(void)searchBarDidLoad
{
    __BLOCKSELF;


    //
    // HIDE SEARCH BAR
    //
    self.tableView.contentOffset = CGPointMake(0, self.searchBar.frame.size.height);
    
    
    
    [self.searchBar setSearchBarSearchButtonClickedBlock:^(UISearchBar *searchBar) {
        
        NSLog(@"tableView.bounds.origin.y=[%f] searchBar.frame.size.height=[%f]",
              __self.tableView.bounds.origin.y, searchBar.frame.size.height);
        
        [searchBar resignFirstResponder];

        addUIKeyboardDidHideObserverAndRemoveOnHandle(^(NSNotification *note) {

            [UIView animateWithDuration:0.3 animations:^(void){
                
                __self.tableView.contentOffset = CGPointMake(0, (__self.tableView.bounds.origin.y + searchBar.frame.size.height  ));
                
            }];
        });
        
        //dispatch_async(dispatch_get_main_queue(), ^{
        
        __self.isSearchActive = YES; __self.jsonDataSearch = __self.jsonData;
        
        __self.jsonData = nil;
        [__self.dataSource removeAllObjects];
        [__self.tableView reloadData];
        [__self.tableView triggerInfiniteScrolling];
        
        //});

        
        
    }];
    
    
    [self.searchBar setSearchBarCancelButtonClickedBlock:^(UISearchBar *searchBar) {
        
        [searchBar resignFirstResponder];

        addUIKeyboardDidHideObserverAndRemoveOnHandle(^(NSNotification *note) {

            [UIView animateWithDuration:0.3 animations:^(void){
                __self.tableView.contentOffset = CGPointMake(0, (__self.tableView.bounds.origin.y + searchBar.frame.size.height  ));
                
            }];
            
        });
        

        if( __self.isSearchActive ) {

            //dispatch_async(dispatch_get_main_queue(), ^{

            [__self resetSearchState];
            
            [__self.tableView reloadData];
            
            //});
        }
    }];
    
}

#pragma mark - Table

-(void)tableDidLoad
{
    __BLOCKSELF;

    [self.tableView setNumberOfSectionsInTableViewBlock:^NSInteger(UITableView *tableView) {
        return 1;
    }];
    
    [self.tableView setNumberOfRowsInSectionBlock:^NSInteger(UITableView *tableView, NSInteger section) {
        return __self.dataSource.count;
    }];
    
    [self.tableView setCellForRowAtIndexPathBlock:^UITableViewCell *(UITableView *tableView, NSIndexPath *indexPath) {
        
        //NSLog(@"indexPath %@", indexPath);
        
        // Doesn't work
        //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Post"];
        
        NSDictionary *object = __self.dataSource[indexPath.row];
        
        //[cell.imageView setImageWithURL:[object valueForKey:@"image"] placeholderImage:[UIImage imageNamed:@"cr@48.jpg"]];
        
        id theImage = object[@"image"];
        
        if (theImage != nil && theImage != [NSNull null]) {
            
            [cell.imageView setImageWithURL:setImageResize(theImage, 200)
                           placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
             {
                 NSLog(@"image.size=[%f,%f]", image.size.width, image.size.height);
             }];
        }
        cell.textLabel.text = object[@"title"];
        
        NSString *when = [object[@"data"] substringWithRange:NSMakeRange(0, 10)];
        
        NSArray *categories = object[@"categories"];
        
        NSString *category = ([categories count] > 0 ) ? categories.firstObject[@"name"] : object[@"type"];
        //NSString *category = [[[object valueForKey:@"categories"] objectAtIndex:0] valueForKey:@"name"];
        
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@   %@", when, category];
        
        return cell;
    }];
    
    [self.tableView setDidSelectRowAtIndexPathBlock:^(UITableView *tableView, NSIndexPath *indexPath) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                NSDate *object = __self.dataSource[indexPath.row];
                
                __self.detailViewController.detailItem = object;
            }
        });
    }];
    
    [self.tableView setCanEditRowAtIndexPathBlock:^BOOL(UITableView *tableView, NSIndexPath *indexPath) {
        return NO;
    }];
    
    [self.tableView setScrollViewDidScroll:^(UIScrollView *tableView) {
        CGRect iAdFrame = [LARSAdController sharedManager].containerView.frame;
        CGFloat newOriginY = tableView.contentOffset.y + tableView.frame.size.height - iAdFrame.size.height;
        CGRect newIAdFrame = CGRectMake(iAdFrame.origin.x, newOriginY, iAdFrame.size.width, iAdFrame.size.height);
        [LARSAdController sharedManager].containerView.frame = newIAdFrame;

        
    }];
    /*
     
     [self.tableView setCommitEditingStyleForRowAtIndexPathBlock:^(UITableView *tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath *indexPath) {
     if (editingStyle == UITableViewCellEditingStyleDelete) {
     [__self.posts removeObjectAtIndex:indexPath.row];
     [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
     } else if (editingStyle == UITableViewCellEditingStyleInsert) {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
     }
     }];
     
     [self.tableView setMoveRowAtIndexPathToIndexPathBlock:^(UITableView *tableView, NSIndexPath *fromIndexPath, NSIndexPath *toIndexPath) {
     
     }];
     
     [self.tableView setCanMoveRowAtIndexPathBlock:^BOOL(UITableView *tableView, NSIndexPath *indexPath) {
     return YES;
     }];
     
     */
    
}

#pragma mark - Controller Lifecycle


- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
    [super awakeFromNib];
    
    UIFont *font = self.navigationController.navigationBar.titleTextAttributes[NSFontAttributeName];
    
    NSAssert( font!=nil, @"NavigationBar font is not set!");
    if( font ) {
        self.navigationItem.titleView = [[BSCTitleView alloc] initWithTitle:self.navigationItem.title
                                                                    theFont:font];
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	
    // Do any additional setup after loading the view, typically from a nib.
    
    __BLOCKSELF;
    
    addSelectCategoryObserver( ^(NSNotification *note) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            BOOL refresh = NO;
            
            if( __self.isSearchActive ) {
                
                [__self resetSearchState];
                
                refresh = YES;
            }

            id object = [note object];
        
            if (object) {
                __self.category = object[@"name"];
                refresh = YES;
            }
            else if (__self.category) {
                __self.category = nil;
                refresh = YES;
            }
            
            NSString *newType = note.userInfo[@"type"];
            
            _typeIsChanged = ![__self.type isEqual:newType];
                
            __self.type = newType;

            if (refresh || _typeIsChanged) {
                __self.jsonData = nil;
                [__self.dataSource removeAllObjects];
                [__self.tableView reloadData];
                [__self.tableView triggerInfiniteScrolling];
                
            }
            
            __self.navigationItem.title = note.userInfo[@"title"];
            [((BSCTitleView *)__self.navigationItem.titleView) setText:note.userInfo[@"title"]];

            
        } );
        
    });
    
    addSelectNewObserver( ^(NSNotification *note) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSNumber *index = note.object;
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[index integerValue]
                                                        inSection:0];
            
            [__self.tableView selectRowAtIndexPath:indexPath
                                          animated:NO
                                    scrollPosition:UITableViewScrollPositionNone];
            
            [__self.navigationController popViewControllerAnimated:NO];
            
            [__self performSegueWithIdentifier:@"showDetail"
                                        sender:self.mainDS];
        } );
        
        
    });
    
    [self searchBarDidLoad];

    [self navigationItemDidLoad];
    
    self.detailViewController = (BSCDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    [self tableDidLoad];
    
    [self pullToRefrehDidLoad];
 
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (![LARSAdController sharedManager].isAdVisible) {
        [[LARSAdController sharedManager] addAdContainerToViewInViewController:self];
    }
    

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];


    double waitForRefresh = 5 * 60.0;
    
    __BLOCKSELF;
    
    [[BSCServices sharedInstance]
     schedulePosts:waitForRefresh
     theType:self.type
     onStart:^{
         
         [__self.loadPostsVC startLoading];
         
     } onCompletion:^(id responseJSON, NSError *error) {
         
         NSLog(@"Schedule Posts %f", CFAbsoluteTimeGetCurrent() );
         
         
         if( error ) {
             NSLog(@"error loading post [%@]", [error description]);
             
             [__self.loadPostsVC endLoading:0];
             return ;
         }
         
         BSCPostData *data = [[BSCPostData alloc] initFormJsonData:responseJSON];
         
         [__self.loadPostsVC endLoading:[__self insertPostsAtTop:data]];
         
         // START NATIVE ANIMATION
         /*
          
          NSTimeInterval duration = 0.25f;
          CGFloat angle = M_PI / 2.0f;
          CGAffineTransform rotateTransform = CGAffineTransformRotate(newsButton.customView.transform, angle);
          
          [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionRepeat| UIViewAnimationOptionCurveLinear animations:^{
          newsButton.customView.transform = rotateTransform;
          } completion:nil];
          
          */
     }];


}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    NSLog(@"viewWillDisappear");
    [[BSCServices sharedInstance] invalidatePostsSheduling];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Storyboard Segue


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        NSDictionary *object = ([sender isKindOfClass:[NSArray class]]) ?
                                                sender[indexPath.row] :
                                                self.dataSource[indexPath.row];
      
#if TARGET_IPHONE_SIMULATOR

        NSMutableDictionary *objectM = [NSMutableDictionary dictionaryWithDictionary:object];
        //objectM[@"id"] = @"15"; // COMMENT TEST
        objectM[@"id"] = @"174492"; // VIDEO ISSUE
        [[segue destinationViewController] setDetailItem:object];
#else
        [[segue destinationViewController] setDetailItem:object];
#endif
    }
}


@end
