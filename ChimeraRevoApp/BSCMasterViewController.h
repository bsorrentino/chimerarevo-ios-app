//
//  BSCMasterViewController.h
//  ChimeraRevoApp
//
//  Created by softphone on 29/01/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BSCDetailViewController;
@class MTBlockTableView;

@interface BSCMasterViewController : UITableViewController <UIScrollViewDelegate>

@property (strong, nonatomic) BSCDetailViewController *detailViewController;
@property (strong, nonatomic) IBOutlet MTBlockTableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


@end
