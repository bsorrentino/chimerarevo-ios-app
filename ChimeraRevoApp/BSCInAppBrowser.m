//
//  BSCInAppBrowser.m
//  ChimeraRevoApp
//
//  Created by softphone on 13/10/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import "BSCInAppBrowser.h"
#import <CDVInAppBrowser.h>
#import <objc/objc.h>
@import ObjectiveC.message;

@implementation BSCInAppBrowser


- (BOOL)webView:(UIWebView*)theWebView shouldStartLoadWithRequest:(NSURLRequest*)request
                                                    navigationType:(UIWebViewNavigationType)navigationType
{
    NSString * absoluteString = [request.URL absoluteString];
    
    NSLog(@"shouldStartLoadWithRequest\n%@", absoluteString);
    
    NSError  *error;
    NSRegularExpression * regex =
        [NSRegularExpression regularExpressionWithPattern:@"http[s]?://redirect.disqus.com/url.*"
                                                  options:NSRegularExpressionCaseInsensitive
                                                    error:&error];
    
    if (!error) {
        
        NSUInteger _matchs = [regex  numberOfMatchesInString:absoluteString
                                               options:0
                                                 range:NSMakeRange(0, absoluteString.length)];
     
        if( _matchs > 0 ) {
            return NO;
        }
        
    }
    else {
        NSLog(@"error [%@]", [error description]);
    }

    SEL selector = NSSelectorFromString(@"webView:shouldStartLoadWithRequest:navigationType:");
    
    if( [super respondsToSelector:selector]) {
        struct objc_super superInfo = {
            self,
            [CDVInAppBrowser class]
        };
        
        BOOL result = (BOOL)objc_msgSendSuper(&superInfo, selector, theWebView, request, navigationType );
        
        return result;
    }
    
    return YES;
}

@end
