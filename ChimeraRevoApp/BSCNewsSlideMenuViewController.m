//
//  BSCNewsSlideMenuViewController.m
//  ChimeraRevoApp
//
//  Created by softphone on 07/02/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <MTBlockTableView/MTBlockTableView.h>
#import "BSCNewsSlideMenuViewController.h"
#import "BSCServices.h"


@interface BSCSection : NSObject

@property(strong,nonatomic) NSDate *date;
@property NSInteger section;
@property NSInteger startIndex;
@property NSInteger endIndex;

-(NSInteger)count;

@end

@implementation BSCSection

-(NSInteger)count
{
    return _endIndex - _startIndex + 1;
}

- (NSString*)description
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:FORMAT_TIMEZONE_ABBREV]];
    [dateFormatter setDateFormat:FORMAT_DATETIME];

    return [NSString stringWithFormat:@"BSCSection{ date=[%@] section = %ld, startIndex = %ld, endIndex = %ld}",
                                        [dateFormatter stringFromDate: self.date],
                                        (long)self.section,
                                        (long)self.startIndex,
                                        (long)self.endIndex];
}
@end


@interface BSCNewsSlideMenuViewController ()

@property (nonatomic,weak)     NSArray *news;
@property (strong,nonatomic,readonly,getter = getDateFormatter) NSDateFormatter *dateFormatter;
@property (nonatomic,strong, getter = getSections)     NSMutableArray/*<Section>*/ *sections;
@property (strong, nonatomic) IBOutlet MTBlockTableView *tableView;
@property () BOOL newsUpdated;


- (void)processNewsToCreateSections;
- (NSDate *)parseDate:(id)object;
- (void)_initialize;

- (BOOL)isVisible;

@end

@implementation BSCNewsSlideMenuViewController
@synthesize dateFormatter=_dateFormatter;

//
// Only to avoid warning from code
//
// [self.rootController performSelector:@selector(doSlideIn:) withObject:nil];
//
-(void)doSlideIn:(id)block
{
    
}

- (BOOL)isVisible {
    return [self isViewLoaded] && self.view.window;
}

- (NSDateFormatter *)getDateFormatter
{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:FORMAT_TIMEZONE_ABBREV]];
        //[_dateFormatter setDateFormat:FORMAT_DATETIME];
    }
    return _dateFormatter;
}

- (NSMutableArray *)getSections
{
    if (!_sections) {
        _sections = [[NSMutableArray alloc] init];
    }
    return _sections;
}

- (NSDate *)parseDate:(id)object
{
    NSAssert(object!=nil, @"object is nil!" );
    if(!object) return nil;
    
    NSString *dateTime = [object valueForKey:@"data"];
    
    NSString *date = [dateTime substringWithRange:NSMakeRange(0,10)];
    
    [self.dateFormatter setDateFormat:FORMAT_DATE];
    
    return [self.dateFormatter dateFromString:date];
}

- (void)processNewsToCreateSections;
{
    NSAssert(self.news!=nil, @"news must be initialized!" );
    if (!self.news) {
        return;
    }
    
    [self.sections removeAllObjects];
    
    if ([self.news count]==0) {
        return;
    }
    // get first and last date
    id first = self.news.firstObject;
    id last = self.news.lastObject;
    
    NSDate *firstDate = [self parseDate:first];
    NSDate *lastDate = [self parseDate:last];
    
    NSTimeInterval diff = [firstDate timeIntervalSinceDate:lastDate];
    
    NSTimeInterval oneDay = 24 *3600;
    
    NSInteger numOfDay = diff / oneDay ;

    NSLog(@"time diff [%f] num of day [%ld]", diff, (long)numOfDay);
    
    NSInteger startIndex = 0;
    NSDate *startDate = firstDate;
    
    for (int i=0; i <= numOfDay && startIndex <  [self.news count] ; ++i) {
        
        BSCSection *section = [[BSCSection alloc] init];
        
        section.date = startDate;
        section.startIndex = startIndex;
        section.endIndex = startIndex;
        section.section = i;
        
        for( ; startIndex < [self.news count]; ) {
            
            id e = [self.news objectAtIndex:startIndex];
            startDate = [self parseDate:e];
            
            if ([section.date isEqualToDate:startDate]) {
                
                section.endIndex = startIndex++;
                continue;
            }

            break;
        }
        
        [self.sections addObject:section];
    }
    
    
}
- (void)_initialize
{
    __BLOCKSELF;
    
    addRefreshNewsObserver(^(NSNotification *notification) {
        
        __self.news = notification.object;
        
        [__self processNewsToCreateSections];
        
        NSLog(@"news = %@", __self.sections);
        
        __self.newsUpdated = YES;
        if (__self.isVisible) {
            [__self.tableView reloadData];
            __self.newsUpdated = NO;
        }
        
        
    });
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self _initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self _initialize];
    }
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.newsUpdated = NO;
    
    __BLOCKSELF;
    
    [self.tableView setNumberOfSectionsInTableViewBlock:^NSInteger(UITableView *tableView) {
        
        NSInteger numSections = [__self.sections count];
        
        NSLog(@"numSections = [%ld]", (long)numSections);
        
        return numSections;
    }];
    
    [self.tableView setNumberOfRowsInSectionBlock:^NSInteger(UITableView *tableView, NSInteger section) {
        
        BSCSection *sectionData = __self.sections[section];
        
        return [sectionData count];
    }];
    
    [self.tableView setTitleForHeaderInSectionBlock:^NSString *(UITableView *tableView, NSInteger section) {
        
        [__self.dateFormatter setDateFormat:FORMAT_DATE];

        BSCSection *sectionData = __self.sections[section];
        
        NSString *title = [__self.dateFormatter stringFromDate:sectionData.date];
        
        NSLog(@"section[%ld]=[%@]", (long)section, title);
        
        return title;
    }];
    
    [self.tableView setHeightForHeaderInSectionBlock:^CGFloat(UITableView *tableView, NSInteger section) {
        return 15.0;
    }];
    
    [self.tableView setCellForRowAtIndexPathBlock:^UITableViewCell *(UITableView *tableView, NSIndexPath *indexPath) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"News"];
        
        BSCSection *sectionData = __self.sections[indexPath.section];
        
        NSDictionary *item = __self.news[sectionData.startIndex + indexPath.row];
        
        cell.textLabel.text = [item valueForKey:@"title"];
        //cell.imageView.image = img;
        return cell;
    }];
    
    [self.tableView setDidSelectRowAtIndexPathBlock:^(UITableView *tableView, NSIndexPath *indexPath) {
        
        BSCSection *sectionData = __self.sections[indexPath.section];

        postSelectNewAtIndex(sectionData.startIndex + indexPath.row);
        
        double delayInSeconds = 0.25;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^{
            
            //
            // SLIDE IN LEFT MENU
            //
            [__self.rootController performSelector:@selector(doSlideIn:) withObject:nil];
            //
            
            //[__self.tableView deselectRowAtIndexPath:indexPath animated:NO];
        });
        
        
        
    }];
    
    [self.tableView setCanEditRowAtIndexPathBlock:^BOOL(UITableView *tableView, NSIndexPath *indexPath) {
        return NO;
    }];

}

- (void)viewWillAppear:(BOOL)animated
{
    // Adjust Header
    UIEdgeInsets inset = UIEdgeInsetsMake(40, 0, 0, 0);
    self.tableView.contentInset = inset;

    if (self.newsUpdated) {
        [self.tableView reloadData];
        self.newsUpdated = NO;
    }
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
