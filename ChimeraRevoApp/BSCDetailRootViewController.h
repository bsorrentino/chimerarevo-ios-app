//
//  BSCDetailRootViewController.h
//  ChimeraRevoApp
//
//  Created by softphone on 20/02/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Cordova/CDVViewController.h>
#import <DCCommentView/DCCommentView.h>

@interface BSCDetailRootViewController : CDVViewController<UIActivityItemSource,DCCommentViewDelegate>

@property (nonatomic, strong) id detailItem;

@end
