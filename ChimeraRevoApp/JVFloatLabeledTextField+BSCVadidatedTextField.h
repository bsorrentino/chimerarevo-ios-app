//
//  UITextField+BSCVadidatedTextField.h
//  ChimeraRevoApp
//
//  Created by Bartolomeo Sorrentino on 17/05/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JVFloatLabeledTextField/JVFloatLabeledTextField.h>

/*
 Validation blocks
 */
typedef BOOL(^ValidationBlock)(NSString *);
typedef void(^PostValidationBlock)(BOOL);

@interface JVFloatLabeledTextField (BSCVadidatedTextField)

/*
 Validation blocks, called in textField:ShouldChangeCharactersInRange:ReplacementString:
 */
@property (strong, nonatomic,getter=getValidationBlock) ValidationBlock validationBlock;
@property (strong, nonatomic, getter=getPostValidationBlock) PostValidationBlock postValidationBlock;
@property(nonatomic,assign) id<UITextFieldDelegate> delegate;


/*
 Setting this method will trigger a postValidationBlock call, if set
 */
@property (nonatomic, getter=getValid) BOOL valid;

/**
 * Revalidates the textfield against it's current text
 */
- (void)revalidate;

- (instancetype)validationSetup:(BOOL(^)(NSString *))validationBlock postValidation:(void(^)(BOOL))postValidationBlock;

@end




