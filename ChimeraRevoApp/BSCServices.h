//
//  BSCServices.h
//  ChimeraRevoApp
//
//  Created by softphone on 29/01/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MBRequest/MBRequest.h>
#import "BSCUserPreferences.h"

//extern  NSString * const selectCategoryNotification;
#define SELECTCATEGORY_NOTIFICATION     @"_selectCategory_"
#define SELECTNEW_NOTIFICATION          @"_selectNew_"
#define REFRESHNEWS_NOTIFICATION        @"_refreshNews_"
#define SELECTTYPE_NOTIFICATION         @"_selectType_"

#define TYPE_POSTS       @"post"
#define TYPE_PRODUCTS    @"prodotti"
#define TYPE_REVIEWS     @"recensioni"
#define TYPE_SPECIALS    @"speciali"
#define TYPE_VIDEOS      @"video"


typedef void (^NotificationHandler)(NSNotification *note);

@interface BSCServices : NSObject

@property (nonatomic, strong, readonly) NSDateFormatter *dateFormatter;

+(instancetype)sharedInstance;

- (void) loadPostsByPage:(NSInteger)page
                 theType:(NSString*)type
             theCategory:(NSString*)category
              theHandler:(MBJSONRequestCompletionHandler)handler ;

- (void) loadPostById:(NSInteger)page
               postId:(NSString *)theId
           theHandler:(MBJSONRequestCompletionHandler)handler;

- (void)loadPostsByUrl:(NSURL *)url handler:(MBJSONRequestCompletionHandler)handler;

- (void) searchPostsByPage:(NSInteger)page
                 theQuery:(NSString*)q
             theCategory:(NSString*)category
              theHandler:(MBJSONRequestCompletionHandler)handler ;

- (void)schedulePosts:(NSTimeInterval)ti
              theType:(NSString*)type
              onStart:(void (^)())startHandler
           onCompletion:(MBJSONRequestCompletionHandler)completionHandler ;
- (void)invalidatePostsSheduling;

- (void)postComment:(NSString *)postId parent:(NSString*)parentId content:(NSString*)content handler:(MBJSONRequestCompletionHandler)handler;

@end

static inline id addUIKeyboardDidHideObserver( NotificationHandler handler  )
{
    return [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardDidHideNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:handler];
    
}
static inline void addUIKeyboardDidHideObserverAndRemoveOnHandle( NotificationHandler handler  )
{
    __block id  observer = [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardDidHideNotification
                                                             object:nil
                                                              queue:[NSOperationQueue mainQueue]
                                                         usingBlock:^(NSNotification *note) {
                                                             [[NSNotificationCenter defaultCenter] removeObserver:observer];
                                                             handler(note);
                                                         }];
    
}

static inline void addSelectCategoryObserver( NotificationHandler handler  )
{
    [[NSNotificationCenter defaultCenter] addObserverForName:SELECTCATEGORY_NOTIFICATION
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:handler];
    
}

static inline void postSelectCategory( NSString *title, NSString *type, id object )
{
    [[NSNotificationCenter defaultCenter] postNotificationName:SELECTCATEGORY_NOTIFICATION
                                                        object:object
                                                      userInfo:@{ @"title":title, @"type":type }
     ];
    
}

static inline void addSelectNewObserver( NotificationHandler handler  )
{
    [[NSNotificationCenter defaultCenter] addObserverForName:SELECTNEW_NOTIFICATION
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:handler];
    
}

static inline void postSelectNewAtIndex( NSInteger index )
{
    [[NSNotificationCenter defaultCenter] postNotificationName:SELECTNEW_NOTIFICATION
                                                        object:[NSNumber numberWithLong:index]
                                                       
     ];
    
}

static inline void addRefreshNewsObserver( NotificationHandler handler  )
{
    [[NSNotificationCenter defaultCenter] addObserverForName:REFRESHNEWS_NOTIFICATION
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:handler];
    
}

static inline void postRefreshNews( id object )
{
    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESHNEWS_NOTIFICATION
                                                        object:object ];
    
}


static inline NSURL * setImageResize( id imageUrlString, int size )
{
    assert(imageUrlString!=nil && imageUrlString != [NSNull null]  );
    assert(size>0);
    
    if( imageUrlString == nil || imageUrlString==[NSNull null] ) return nil;
    
    NSURL *imageUrl = [[NSURL alloc] initWithString:imageUrlString];
    
    if( !imageUrl.scheme || !imageUrl.host ) return nil;

    if( size <=0 ) return imageUrl;

    
    NSURL *url = [[NSURL alloc] initWithScheme:imageUrl.scheme
                                          host:imageUrl.host
                                          path:[[NSString stringWithFormat:@"%@?resize=%d%%2c%d",imageUrl.path, size, size] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    //NSLog(@"URL %@", url );
    
    return url;
    
}

