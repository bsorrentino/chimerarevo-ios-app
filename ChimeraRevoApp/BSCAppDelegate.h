//
//  BSCAppDelegate.h
//  ChimeraRevoApp
//
//  Created by softphone on 29/01/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MDDisqusComponent;

@interface BSCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, readonly) MDDisqusComponent *disqusComponent;

@end
