//
//  main.m
//  ChimeraRevoApp
//
//  Created by softphone on 29/01/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BSCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BSCAppDelegate class]));
    }
}
