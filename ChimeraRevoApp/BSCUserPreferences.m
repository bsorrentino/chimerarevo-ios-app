//
//  BSCPreferences.m
//  ChimeraRevoApp
//
//  Created by Bartolomeo Sorrentino on 05/04/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import "BSCUserPreferences.h"
#import "BSCServices.h"
@implementation BSCUserPreferences

@dynamic lastPostDateAsString;
@dynamic username;
@dynamic mail;

-(NSDate *)lastPostDate {
    
    [[BSCServices sharedInstance].dateFormatter setDateFormat:POST_DATE_FORMAT];
    
    return [[BSCServices sharedInstance].dateFormatter dateFromString:self.lastPostDateAsString];
    //return [NSDate dateWithTimeIntervalSince1970:self.lastPostDateInMills];
}

@end
