//
//  BSCDetailRootViewController.m
//  ChimeraRevoApp
//
//  Created by softphone on 20/02/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <UIViewController+HUD/UIViewController+HUD.h>
#import "BSCDetailRootViewController.h"
#import "BSCCommentAccountViewController.h"
#import "BSCServices.h"

#import <CordovaPlugin-inappbrowser/CDVInAppBrowser.h>

#define __SUPPORT_BADGE 1

#if __SUPPORT_BADGE
#import <BBBadgeBarButtonItem/BBBadgeBarButtonItem.h>
#endif

@interface BSCDetailRootViewController ()

@property (strong,nonatomic) DCCommentView *commentView;
@property (strong,nonatomic) NSString *replyId;

- (void)commentDidLoad;
- (void)navigationItemDidLoad;
- (void)share:(id)sender;
- (void)commentAdded:(NSDictionary *)userInfo;
@end

@implementation BSCDetailRootViewController {
}

static id _replyObserver;
#if __SUPPORT_BADGE
static id _countObserver;
#endif

#pragma mark - Comments Management

- (void)commentAdded:(NSDictionary *)userInfo
{
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
                                                       options:0 // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", [error description]);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [self.webView
         stringByEvaluatingJavaScriptFromString:
         [NSString stringWithFormat:@"JSB.invokeHandler( 'comment.added', %@ );",  jsonString]];
    }
    
}

- (void)commentDidLoad
{
    __BLOCKSELF;
    
    self.commentView.tintColor = [UIColor redColor];
    //self.commentView.accessoryImage = [UIImage imageNamed:@"cr@48.jpg"];
    
    if( !_replyObserver) {
        _replyObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"comment.reply"
                                                          object:nil
                                                           queue:nil
                                                      usingBlock:^(NSNotification *note) {

                    NSLog(@"Reply handled [%@]", [note userInfo]);
                    
                    __self.replyId = [note userInfo][@"replyId"];

                    [__self performSegueWithIdentifier:@"login" sender:self];
            
        }];
    }
   
}

- (DCCommentView *)commentView
{
    if(!_commentView) {
        _commentView = [[DCCommentView alloc] init];
        _commentView.delegate = self;
    }
    
    return _commentView;
}

- (void)comment:(id)sender
{
    self.replyId = nil;
    
    //[self performSegueWithIdentifier:@"login" sender:self];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:@{ @"url":self.detailItem[@"permalink"], @"title":self.detailItem[@"title"]}
                                                       options:0 // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", [error description]);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [self.webView
         stringByEvaluatingJavaScriptFromString:
         [NSString stringWithFormat:@"JSB.invokeHandler( 'comment.open', %@ );",  jsonString]];
    }

    
}

-(void)didSendComment:(NSString*)text
{
    __BLOCKSELF;
    
    __block NSString * postId = self.detailItem[@"id"];
    __block NSString * replyId = self.replyId;
    
    [self.commentView resignFirstResponder];
    [self showHudWithTitle:@"Inserimento Commento" andSubtitle:@"Invio richiesta ...." animated:YES];
    
    [[BSCServices sharedInstance ] postComment:postId
                                        parent:replyId
                                       content:text
                                       handler:^(id responseJSON, NSError *error)
    {

           if( !error ) {
               
               NSMutableDictionary *info =
                [NSMutableDictionary dictionaryWithDictionary:@{ @"postId":postId, @"content":text}];
               
               if(  replyId ) {
                   [info setValue:replyId forKeyPath:@"parent"];
               }
               
               [__self commentAdded:info];
               
               __self.hud.mode = MBProgressHUDModeText;
               
               [__self showHudWithTitle:@"Il commento è stato inserito!"
                            andSubtitle:@"Sarà disponibile dopo un breve processo di verifica"
                               animated:NO
                ];
          }
           else {
               __self.hud.mode = MBProgressHUDModeText;
               
               [__self showHudWithTitle:@"Il commento NON è stato inserito!"
                            andSubtitle:@"Si è verificato un problema. Riprova più tardi!"
                               animated:NO
                ];
               
           }

           __self.replyId = nil;
        
            if(__self.hud) {
                [__self.hud hide:YES afterDelay:2.5F];
                [__self setHud:nil];
            }
    }];
    
    
}

#pragma mark - Private Methods


- (void)share:(id)sender
{
    
    NSURL *permalink = [NSURL URLWithString:self.detailItem[@"permalink"]];
    NSString *title = self.detailItem[@"title"];
    
    NSAssert(permalink!=nil, @"permalink is null!");
    if( !permalink ) return;
    
    if (!title) { title = @""; }
    
    __block NSMutableArray *activityItems = [NSMutableArray arrayWithArray:@[title,permalink]];
    
    SDWebImageCompletedWithFinishedBlock onCommpleted =
        ^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {

            dispatch_main_sync_safe(^{
                if (image) {
                    [activityItems addObject:image];
                }
            });
            
        };

    id theImage = self.detailItem[@"image"];
    
    if( !theImage ) {
        
        theImage =  self.detailItem[@"video_thumb"];
    }
    
    
    if( theImage) {
        /*id <SDWebImageOperation> operation = */
        [SDWebImageManager.sharedManager downloadWithURL:setImageResize(theImage, 200)
                                                 options:0
                                                progress:nil
                                               completed:onCommpleted];
    }
    
    UIActivityViewController *activityController =
        [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                          applicationActivities:nil];
    activityController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    [activityController setValue:@"Articolo da Chimera Revo" forKey:@"subject"];
    
    [self presentViewController:activityController animated:YES completion:nil];
}


- (void)navigationItemDidLoad
{
    
    
    UIBarButtonItem *shareButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                  target:self
                                                  action:@selector(share:)];
    
#if __SUPPORT_BADGE
    
    UIImage *commentImage = [UIImage imageNamed:@"comment-icon"];
    // If you want your BarButtonItem to handle touch event and click, use a UIButton as customView
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    // Add your action to your button
    [customButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
    // Customize your button as you want, with an image if you have a pictogram to display for example
    [customButton setImage:commentImage forState:UIControlStateNormal];
    
    // Then create and add our custom BBBadgeBarButtonItem
    __block BBBadgeBarButtonItem *commentButton = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:customButton];
    // Set a value for the badge
    
    commentButton.badgeOriginX = 27;
    commentButton.badgeOriginY = 0;
    commentButton.shouldHideBadgeAtZero = YES;
    commentButton.shouldAnimateBadge = YES;
    
    if( !_countObserver ) {
        _countObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"comment.count"
                                                                           object:nil
                                                                            queue:nil
                                                                       usingBlock:^(NSNotification *note) {
           
                                id count = [note userInfo][@"count"];
                                                                                                      
                                NSLog(@"Count handled [%@]", [note userInfo]);
                                      
                                commentButton.badgeValue =  [count stringValue];
           
                        }];
    }
    
#else

    UIBarButtonItem *commentButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose
                                                  target:self
                                                  action:@selector(comment:)];
#endif
    
    [self.navigationItem setRightBarButtonItems:@[ shareButton, commentButton ]];
    
    
    
    
}

#pragma mark -
#pragma mark UIViewController lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self navigationItemDidLoad];
    
    [self commentDidLoad];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    __BLOCKSELF;
    
    if (self.detailItem ) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSNumber *postId = __self.detailItem[@"id"];
            
            NSString *methodCall = [NSString stringWithFormat:@"loadPage('%@')", postId ];
            
            NSString *result = [__self.webView stringByEvaluatingJavaScriptFromString:methodCall];
            
            NSLog(@"%@ = %@", methodCall, result);
            
            //__self.detailItem = nil;
        });
    }

    [super viewDidAppear:animated];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.commentView resignFirstResponder];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    //self.detailItem = nil;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setDetailItem:(id)item
{
    _detailItem = item;
}

-(void)dealloc {
#if __SUPPORT_BADGE
    [[NSNotificationCenter defaultCenter] removeObserver:_countObserver];
#endif
    [[NSNotificationCenter defaultCenter] removeObserver:_replyObserver];
    
    _countObserver = nil;
    _replyObserver = nil;
}

#pragma mark - UIwebViewDelegate implementation

#if 0
- (BOOL) webView:(UIWebView*)theWebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"shouldStartLoadWithRequest\n\t[%@] [%ld]", [request.URL relativeString], navigationType );
    
    if (navigationType!=UIWebViewNavigationTypeOther /*&& host && [host compare:@"disqus.com"]==NSOrderedSame*/) {
        
        return NO;
        
    }
    
    return [super webView:theWebView shouldStartLoadWithRequest:request navigationType:navigationType];
}
#endif

#pragma mark - UIActivityItemSource

// UIActivityItemSource customization
// see: http://stackoverflow.com/questions/12984403/uiactivityviewcontroller-email-and-twitter-sharing

// called to determine data type. only the class of the return type is consulted. it should match what -itemForActivityType: returns later
- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return nil;
}
// called to fetch data after an activity is selected. you can return nil.
- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    return nil;
}

#pragma mark -
#pragma mark UIViewController storyboard

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    __BLOCKSELF;
   
    if ([[segue identifier] isEqualToString:@"login"]) {
        
         [[segue destinationViewController] setOnSave:^{
             
              if( ![__self.commentView isFirstResponder]) {
              [__self.commentView bindToScrollView:__self.webView.scrollView superview:__self.view];
              [__self.commentView becomeFirstResponder];
              }
             
         }];
        
         [[segue destinationViewController ] setOnCancel:^{
         }];
    }
    
}


@end
