//
//  UITextField+BSCVadidatedTextField.m
//  ChimeraRevoApp
//
//  Created by Bartolomeo Sorrentino on 17/05/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//
#import <objc/runtime.h>
#import "JVFloatLabeledTextField+BSCVadidatedTextField.h"

#define CATEGORY_PROPERTY( type ) \
static char name##Key; \
- (type*)get##type\
{\
    return objc_getAssociatedObject(self, & name##Key);\
}\
- (void)set##type:(type*)value\
{\
    objc_setAssociatedObject(self, & name##Key, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);\
}

#define CATEGORY_PROPERTY2( type, name ) \
static char name##Key; \
- (type)get##name\
{\
return objc_getAssociatedObject(self, & name##Key);\
}\
- (void)set##name:(type)value\
{\
objc_setAssociatedObject(self, & name##Key, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);\
}

#define CATEGORY_PROPERTY_BOOL( name ) \
static char name##Key; \
- (BOOL)get##name\
{ \
NSNumber *result = objc_getAssociatedObject(self, & name##Key);\
return (result) ? [result boolValue] : NO; \
}\
- (void)set##name:(BOOL)value\
{\
NSNumber *v = [NSNumber numberWithBool:value]; \
objc_setAssociatedObject(self, & name##Key, v, OBJC_ASSOCIATION_RETAIN_NONATOMIC);\
}

/*
 Unfotunately a UITextField cannot be its own delegate, due to a infinite loop in [self respondsToSelector]
 because of this, delegate methods are routed through another object
 Reference: http://www.cocoabuilder.com/archive/cocoa/241465-iphone-why-can-a-uitextfield-be-its-own-delegate.html#241505
 */
@interface DelegateRouter : NSObject<UITextFieldDelegate>
@property JVFloatLabeledTextField *textField;

- (id)initWithValidatingTextField:(JVFloatLabeledTextField *)textField;

@end


@interface JVFloatLabeledTextField ()
@property (strong, nonatomic, getter=getDelegateRouter) DelegateRouter *delegateRouter;

/**
 * The forward delegate for this tableView
 *
 * This UITextField implements some methods of UITextFieldDelegate to assist in validation, however
 * since we still want the user to be able to use a UITextField delegate, we use this to store the user's
 * delegate and forward methods calls to it.
 */
@property (nonatomic, assign, getter=getForwardDelegate) id <UITextFieldDelegate> forwardDelegate;


@end


@implementation JVFloatLabeledTextField (BSCVadidatedTextField)
@dynamic delegate;

CATEGORY_PROPERTY(DelegateRouter);
CATEGORY_PROPERTY2(id<UITextFieldDelegate>, ForwardDelegate);
CATEGORY_PROPERTY2(ValidationBlock, ValidationBlock);
CATEGORY_PROPERTY2(PostValidationBlock, PostValidationBlock);
CATEGORY_PROPERTY_BOOL(Valid);


/**
 * Sets up the delegate to be the DelegateRouter
 */
- (instancetype)validationSetup:(BOOL(^)(NSString *))validationBlock postValidation:(void(^)(BOOL))postValidationBlock;
{
    // Init the delegate router with this text field
    self.delegateRouter = [[DelegateRouter alloc] initWithValidatingTextField:self];
    
    if (self.delegate) {
        self.forwardDelegate = self.delegate;
    }
    // Set the delegate on super to enable use of the forward delegate
    self.delegate = self.delegateRouter;
    
    [self setValidationBlock:validationBlock];
    [self setPostValidationBlock:postValidationBlock];
    return self;
}

/*
- (void)setValid:(BOOL)valid
{
    self.valid = valid;
    if ( self.postValidationBlock ) {
        self.postValidationBlock( self.valid);
    }
}

- (BOOL)valid
{
    return self.valid;
}
*/
- (void)dealloc
{
    self.delegate = nil;
    self.delegateRouter.textField = nil;
    self.self.delegateRouter = nil;
    self.validationBlock = nil;
    self.postValidationBlock = nil;
}


/**
 * Validates the current field against a given string
 */
- (void)validateAgainstString:(NSString *)string
{
    if ( self.validationBlock != nil ) {
        self.valid = self.validationBlock(string);
        if ( self.postValidationBlock != nil ) {
            self.postValidationBlock(self.valid);
        }
    }
}

- (void)revalidate {
    [self validateAgainstString:self.text];
}


@end


@implementation DelegateRouter

/**
 Editing the Text Field's Text
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self.textField validateAgainstString:[self.textField.text stringByReplacingCharactersInRange:range withString:string]];
    
    if ([self.textField.forwardDelegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)])
    {
        return [self.textField.forwardDelegate textField:self.textField shouldChangeCharactersInRange:range replacementString:string];
    }
    else
    {
        return YES;
    }
}

- (id)initWithValidatingTextField:(JVFloatLabeledTextField *)textField
{
    self = [super init];
    self.textField = textField;
    return self;
}

- (void)dealloc
{
    self.textField = nil;
}

#pragma mark - Delegate Forwarding

/**
 * Returns YES if DelegateRouter or the ForwardDelegate respond to the selector
 */
- (BOOL)respondsToSelector:(SEL)aSelector
{
    return [super respondsToSelector:aSelector] || [self.textField.forwardDelegate respondsToSelector:aSelector];
}

/**
 * Pass off any methods not implemented by DelegateRouter to the delegate the user set on self.textField
 */
- (id)forwardingTargetForSelector:(SEL)aSelector
{
    if ([super respondsToSelector:aSelector]) return self;
    if ([self.textField.forwardDelegate respondsToSelector:aSelector]) return self.textField.forwardDelegate;
    
    return nil;
}



@end

