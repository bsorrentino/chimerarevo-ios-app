//
//  BSCNavigationController.h
//  ChimeraRevoApp
//
//  Created by softphone on 11/06/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSCTitleView : UIView

@property (nonatomic,strong,setter=setText:) NSString *text;

- (id)initWithTitle:(NSString *)value theFont:(UIFont *)font;

@end

