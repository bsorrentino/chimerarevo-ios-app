//
//  BSCSlideMenuViewController.m
//  ChimeraRevoApp
//
//  Created by softphone on 07/02/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <MTBlockTableView/MTBlockTableView.h>
#import <SASlideMenu/SASlideMenuRootViewController.h>

#import "BSCCategorySlideMenuViewController.h"
#import "BSCServices.h"


@interface BSCCategorySlideMenuViewController ()

@property (strong, nonatomic) IBOutlet MTBlockTableView *tableView;
@property (strong, nonatomic, getter = getCategories) NSArray *dataSource;

- (void)tableViewDidLoad;
- (void)selectHeader:(UITapGestureRecognizer *)sender;

@end

@implementation BSCCategorySlideMenuViewController

//
// Only to avoid warning from code
//
// [self.rootController performSelector:@selector(doSlideIn:) withObject:nil];
//
-(void)doSlideIn:(id)block
{
    
}

- (NSArray *)getCategories
{
    if( !_dataSource ) {
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"categories" ofType:@"json"];
        NSData *myData = [NSData dataWithContentsOfFile:filePath ];
        
        NSError *error;
        _dataSource = [NSJSONSerialization JSONObjectWithData:myData options:kNilOptions error:&error];
        
        NSAssert(!error, @"error occurred loading catogories json data [%@]", [error description] );
        if (error) {
            NSLog( @"error occurred loading catogories json data [%@]", [error description]);
            _dataSource = @[];
        }
        
    }
    return _dataSource;
}


- (void)notificationDidLoad
{
    [NSNotificationCenter defaultCenter] ;
}

- (void)selectHeader:(UITapGestureRecognizer *)sender {

    NSLog(@"selectHeader %ld", (long)sender.view.tag );

    postSelectCategory( self.dataSource[sender.view.tag][@"title"],
                        self.dataSource[sender.view.tag][@"type"],
                        nil ); // Reset Filter
    
    __BLOCKSELF;
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^{
        
        //
        // SLIDE IN LEFT MENU
        //
        [__self.rootController performSelector:@selector(doSlideIn:) withObject:nil];
        //
    });

    
}

- (void)tableViewDidLoad
{

    __BLOCKSELF;
    
    UIEdgeInsets inset = UIEdgeInsetsMake(40, 0, 0, 0);
    self.tableView.contentInset = inset;
    self.tableView.bounces = NO;
    self.tableView.sectionHeaderHeight = 0.5;
    self.tableView.sectionFooterHeight = 0.5;
    
    [self.tableView setNumberOfSectionsInTableViewBlock:^NSInteger(UITableView *tableView) {
        
        NSInteger numSections = [__self.dataSource count];
        
        NSLog(@"numSections = [%ld]", (long)numSections);

        return numSections;
    }];
    
    [self.tableView setNumberOfRowsInSectionBlock:^NSInteger(UITableView *tableView, NSInteger section) {
        
        NSDictionary *sectionData = __self.dataSource[section];
        
        NSArray *items = [sectionData valueForKey:@"items"];
        
        return [items count];
    }];
    
    [self.tableView setTitleForHeaderInSectionBlock:^NSString *(UITableView *tableView, NSInteger section) {
        NSDictionary *sectionData = __self.dataSource[section];
        
        NSString *title = [sectionData valueForKey:@"title"];
        
        NSLog(@"section[%ld]=[%@]", (long)section, title);
        
        return title;
    }];
    
    [self.tableView setCellForRowAtIndexPathBlock:^UITableViewCell *(UITableView *tableView, NSIndexPath *indexPath) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Category2"];
        
        NSDictionary *sectionData = __self.dataSource[indexPath.section];
        
        NSArray *items = [sectionData valueForKey:@"items"];
        
        NSString *text = [items[indexPath.row] valueForKey:@"name"];

        
        UIImage *img = [UIImage imageNamed:text];
        cell.imageView.image = img;
        
        
        cell.textLabel.text = text;
        return cell;
    }];
    
    [self.tableView setDidSelectRowAtIndexPathBlock:^(UITableView *tableView, NSIndexPath *indexPath) {
        
        
        postSelectCategory( __self.dataSource[indexPath.section][@"items"][indexPath.row][@"name"],
                            __self.dataSource[indexPath.section][@"type"],
                            __self.dataSource[indexPath.section][@"items"][indexPath.row]);

        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^{
            
            //
            // SLIDE IN LEFT MENU
            //
            [__self.rootController performSelector:@selector(doSlideIn:) withObject:nil];
            //
            
            [__self.tableView deselectRowAtIndexPath:indexPath animated:NO];
        });
            
        
    }];
    
    [self.tableView setCanEditRowAtIndexPathBlock:^BOOL(UITableView *tableView, NSIndexPath *indexPath) {
        return NO;
    }];

    [self.tableView setHeightForHeaderInSectionBlock:^CGFloat(UITableView *tableView, NSInteger section) {
        return 50.5;
    }];
    
    [self.tableView setViewForHeaderInSectionBlock:^UIView *(UITableView *tableView, NSInteger section) {
        UILabel *result = [[UILabel alloc] init];
        [result setTextColor:[UIColor whiteColor]];
        
        result.userInteractionEnabled = YES;
        UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:__self
                                                                                      action:@selector(selectHeader:)];
        
        result.tag = section;
        [result addGestureRecognizer:recognizer];
        [result setBackgroundColor:[UIColor colorWithRed:0.816 green:0.090 blue:0.086 alpha:1.000]];
        
        result.text = [NSString stringWithFormat:@"   %@", ((MTBlockTableView *)tableView).titleForHeaderInSectionBlock(tableView,section) ];
        
        return result;
    }];
    
     /*
     [self.tableView setCommitEditingStyleForRowAtIndexPathBlock:^(UITableView *tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath *indexPath) {
     if (editingStyle == UITableViewCellEditingStyleDelete) {
     [__self.posts removeObjectAtIndex:indexPath.row];
     [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
     } else if (editingStyle == UITableViewCellEditingStyleInsert) {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
     }
     }];
     
     [self.tableView setMoveRowAtIndexPathToIndexPathBlock:^(UITableView *tableView, NSIndexPath *fromIndexPath, NSIndexPath *toIndexPath) {
     
     }];
     
     [self.tableView setCanMoveRowAtIndexPathBlock:^BOOL(UITableView *tableView, NSIndexPath *indexPath) {
     return YES;
     }];
     
     */
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    [self tableViewDidLoad];
    
    //[self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark SASlideMenuDataSource

-(NSIndexPath*) selectedIndexPath{
    return [NSIndexPath indexPathForRow:0 inSection:0];
}

-(NSString*) segueIdForIndexPath:(NSIndexPath *)indexPath{
    return @"Post";
}

-(void) configureMenuButton:(UIButton *)menuButton{
    menuButton.frame = CGRectMake(0, 0, 40, 29);
    [menuButton setImage:[UIImage imageNamed:@"menuicon"] forState:UIControlStateNormal];
}


-(Boolean) hasRightMenuForIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

/*
-(void)configureRightMenuButton:(UIButton *)menuButton {

    //menuButton.frame = CGRectMake(0, 0, 40, 29);
    //[menuButton setImage:[UIImage imageNamed:@"menuicon"] forState:UIControlStateNormal];
    
}
 
 */

-(CGFloat) leftMenuVisibleWidth{
    return 260;
}


-(CGFloat) rightMenuVisibleWidth{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    return screenWidth - 42;
}


@end
