//
//  BSCPostData.h
//  ChimeraRevoApp
//
//  Created by softphone on 30/03/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSCPostData : NSObject

@property (nonatomic,weak,getter=getPosts,readonly)       NSArray *posts;
@property (nonatomic,getter=getTotalPages,readonly)       NSInteger totalPages;
@property (nonatomic,getter=getPage,readonly)             NSInteger page;
@property (nonatomic,weak,getter=getNextPage,readonly)    NSURL *nextPage;
@property (nonatomic,weak,getter=getPrevPage,readonly)    NSURL *prevPage;

-(instancetype)initFormJsonData:(id)jsonData;

- (NSArray *)selectLatest:(id)post;
@end


extern const NSString * BSCLinkNextPageRel ;
extern const NSString * BSCLinkPrevPageRel ;
