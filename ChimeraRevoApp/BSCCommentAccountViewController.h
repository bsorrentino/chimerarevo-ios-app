//
//  BSCCommentAccountViewController.h
//  ChimeraRevoApp
//
//  Created by Bartolomeo Sorrentino on 13/05/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSCCommentAccountViewController : UIViewController

+ (instancetype)instanceFromStoryboard:(UIStoryboard *)storyboard;

@property (nonatomic, strong) void (^onCancel)();
@property (nonatomic, strong) void (^onSave)();

@end
