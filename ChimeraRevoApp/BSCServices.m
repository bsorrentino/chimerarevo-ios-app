//
//  BSCServices.m
//  ChimeraRevoApp
//
//  Created by softphone on 29/01/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import "BSCServices.h"

@interface BSCServices ()

@property (nonatomic,strong) NSTimer *timer;

- (void)refreshPosts:(NSTimer *)owner;
- (void) loadPostsByPage:(NSInteger)page theHandler:(MBJSONRequestCompletionHandler)handler;
@end


@implementation BSCServices

@synthesize dateFormatter=_dateFormatter;

+(instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static id sharedInstance;
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [BSCServices new];
    });
    
    return sharedInstance;
}

- (NSDateFormatter *)dateFormatter
{
    if( !_dateFormatter ) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:FORMAT_TIMEZONE_ABBREV]];

    }
    return _dateFormatter;
}

- (void)refreshPosts:(NSTimer *)owner
{
    void (^start)() = owner.userInfo[@"start"];
    
    assert( start!=nil );
    
    if( start ) start();
    
    MBJSONRequestCompletionHandler completion = owner.userInfo[@"completion"];
    
    [self loadPostsByPage:1
                  theType:owner.userInfo[@"type"]
              theCategory:nil
               theHandler:completion];
    
}

- (void) loadPostsByPage:(NSInteger)page theHandler:(MBJSONRequestCompletionHandler)handler
{
    NSString *urlString =
    [NSString stringWithFormat:@"http://www.chimerarevo.com/api/1.0/posts/posts.json?page=%ld&orderBy=date", (long)page] ;
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    [self loadPostsByUrl:url handler:handler];
    
}

- (void)loadPostsByUrl:(NSURL *)url handler:(MBJSONRequestCompletionHandler)handler
{
    assert( url != nil );
    if( !url ) return;
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    MBJSONRequest *jsonRequest = [[MBJSONRequest alloc] init];
    
    [jsonRequest performJSONRequest:urlRequest completionHandler:^(id responseJSON, NSError *error) {
        
        handler( responseJSON, error);
    }];
}

- (void) searchPostsByPage:(NSInteger)page
                  theQuery:(NSString*)q
               theCategory:(NSString*)category
                theHandler:(MBJSONRequestCompletionHandler)handler
{
    NSString *urlString =  (category) ?
    [NSString stringWithFormat:@"http://www.chimerarevo.com/api/1.0/posts/search.json?q=%@&page=%ld&category=%@&orderBy=date",
     q,(long)page, category] :
    [NSString stringWithFormat:@"http://www.chimerarevo.com/api/1.0/posts/search.json?q=%@&page=%ld&orderBy=date",
     q, (long)page];
    
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    [self loadPostsByUrl:url handler:handler];
    
}


- (void) loadPostsByPage:(NSInteger)page
                 theType:(NSString *)type
             theCategory:(NSString*)category
              theHandler:(MBJSONRequestCompletionHandler)handler
{
    NSString *urlString =  (category) ?
        [NSString stringWithFormat:@"http://www.chimerarevo.com/api/1.0/posts/posts.json?type=%@&page=%ld&category=%@&orderBy=date",
            type,(long)page, category] :
        [NSString stringWithFormat:@"http://www.chimerarevo.com/api/1.0/posts/posts.json?type=%@&page=%ld&orderBy=date",
            type, (long)page] ;
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    [self loadPostsByUrl:url handler:handler];
    
}

- (void) loadPostById:(NSInteger)page postId:(NSString *)theId theHandler:(MBJSONRequestCompletionHandler)handler
{
    NSString *urlString =
        [NSString stringWithFormat:@"http://www.chimerarevo.com/api/1.0/posts/post.json?id=%@",  theId] ;
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    [self loadPostsByUrl:url handler:handler];
    
}

- (void)schedulePosts:(NSTimeInterval)ti
              theType:(NSString*)type
              onStart:(void (^)())startHandler
         onCompletion:(MBJSONRequestCompletionHandler)completionHandler
{
    
    if( self.timer && [self.timer isValid] ) {
        return;
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:ti
                                     target:self
                                   selector:@selector(refreshPosts:)
                                   userInfo:@{ @"type": type,
                                               @"start": [startHandler copy],
                                               @"completion": [completionHandler copy] }
                                    repeats:NO];
}


- (void)invalidatePostsSheduling
{
    if (self.timer && [self.timer isValid]) {

        [self.timer invalidate];
        self.timer = nil;
    }
}

#pragma mark - Comment

/**
 
 POST posts/comments
 Crea un commento per il post specificato
 
 Resource URL
 http://www.chimerarevo.com/api/1.0/posts/comments.json
 Parametri
 
 postid     obbligatorio   Id del post
 parent     opzionale      id del commento in risposta
 username   obbligatorio   Nome uente
 email      obbligatorio   Email utente
 url        opzionale      indirizzo web utente
 content    obbligatorio   Contenuto del commento
*/
- (void)postComment:(NSString *)postId parent:(NSString*)parentId content:(NSString*)content handler:(MBJSONRequestCompletionHandler)handler
{
    NSString *postString = [NSString stringWithFormat:@"postid=%@&username=%@&email=%@&content=%@",
                            postId,
                            [BSCUserPreferences sharedInstance].username,
                            [BSCUserPreferences sharedInstance].mail,
                            content
                            ];
    if (parentId) {
        postString = [postString stringByAppendingFormat:@"&parent=%@", parentId ];
    }
    NSData *postBody = [postString dataUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL  URLWithString:@"http://www.chimerarevo.com/api/1.0/posts/comments.json"];

    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPBody:postBody];
    
    MBJSONRequest *jsonRequest = [[MBJSONRequest alloc] init];
    
    [jsonRequest performJSONRequest:urlRequest completionHandler:^(id responseJSON, NSError *error) {
        
        handler( responseJSON, error);
    }];
    
}


@end
