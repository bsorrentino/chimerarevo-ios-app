//
//  BSCLoadPostViewController.h
//  ChimeraRevoApp
//
//  Created by softphone on 04/04/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import "CDVViewController.h"

@interface BSCLoadPostViewController : CDVViewController

- (void)startLoading;

- (void)endLoading:(NSInteger)numOfNews;

@end
