//
//  BSCAppDelegate.m
//  ChimeraRevoApp
//
//  Created by softphone on 29/01/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import "BSCAppDelegate.h"
#import "BSCServices.h"
#import <LARSAdController/LARSAdController.h>
#import <LARSAdController/TOLAdAdapteriAds.h>

#if __USE_DISCUS
#import <Disqus/MDDisqusComponent.h>
#endif

@implementation BSCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    [[LARSAdController sharedManager] registerAdClass:[TOLAdAdapteriAds class]];

    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
        splitViewController.delegate = (id)navigationController.topViewController;
    }

    NSLog(@"lastPostDate [%@]", [BSCUserPreferences sharedInstance].lastPostDateAsString );
    
    
    addRefreshNewsObserver(^(NSNotification *notification) {
        
        if (notification.object && [notification.object count]>0) {
            __block NSDictionary *post = [notification.object objectAtIndex:0];
            
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                
                [BSCUserPreferences sharedInstance].lastPostDateAsString = [post valueForKey:@"data"];
                
            });
        }
        
    });
    
#if __USE_DISCUS
//
// DISQUS
//
    NSString *publicKey = @"gWfG2DJ6EbPrbo5DoO35vdH0IvqqcFtQQShetdYNJwsa0xxtzhqCTGdAi5ETQSok";
    NSString *secretKey = @"yQRc5PVmrmFsnwKeMFg748LLUUUJk7SSHTIlDF8BG9JjmT06nRnZ4VaOH46iHoqJ";
    NSString *redirectURLString = @"http://www.chimerarevo.com/";
    
    _disqusComponent = [[MDDisqusComponent alloc] initWithPublicKey:publicKey secretKey:secretKey redirectURL:[NSURL URLWithString:redirectURLString]];
#endif
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
