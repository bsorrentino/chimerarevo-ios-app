//
//  BSCNewsSlideMenuViewController.h
//  ChimeraRevoApp
//
//  Created by softphone on 07/02/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SASlideMenuViewController.h"
#import "SASlideMenuRightMenuViewController.h"

@interface BSCNewsSlideMenuViewController : SASlideMenuRightMenuViewController


@end
