//
//  BSCCommentAccountViewController.m
//  ChimeraRevoApp
//
//  Created by Bartolomeo Sorrentino on 13/05/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "BSCCommentAccountViewController.h"
#import "JVFloatLabeledTextField+BSCVadidatedTextField.h"
#import "BSCUserPreferences.h"

@interface BSCCommentAccountViewController ()
- (IBAction)cancel:(id)sender;
- (IBAction)save:(id)sender;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *username;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *mail;
@property (weak, nonatomic) IBOutlet UIButton *save;

@end

@implementation BSCCommentAccountViewController

+ (instancetype)instanceFromStoryboard:(UIStoryboard *)storyboard
{
    return [storyboard instantiateViewControllerWithIdentifier:@"BSCCommentAccount"];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    __BLOCKSELF;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.save.layer.cornerRadius=8.0f;
    
    
    //
    // USERNAME
    //
    {
        __block UIImageView *invalidView = [[UIImageView alloc ] initWithImage:[UIImage imageNamed:@"invalid"]];
        {
            invalidView.contentMode = UIViewContentModeLeft; CGRect frame = invalidView.frame; frame.size.width += 2; [invalidView setFrame:frame];
        }
        __block UIImageView *validView = [[UIImageView alloc ] initWithImage:[UIImage imageNamed:@"valid"]];
        {
            validView.contentMode = UIViewContentModeLeft; CGRect frame = validView.frame; frame.size.width += 2; [validView setFrame:frame];
        }
            
        self.username.rightViewMode = UITextFieldViewModeAlways;
        
        self.username.floatingLabelFont = [UIFont boldSystemFontOfSize:9.5];
        [self.username setPlaceholder:@"nome utente" floatingTitle:@"il tuo nick (almeno 4 char)"];
        
        [self.username validationSetup:^BOOL(NSString *value) {
            return [value length] > 3;
        } postValidation:^(BOOL valid) {
            
            __self.username.rightView = (valid) ? validView : invalidView;
        }] ;
    
        [self.username addTarget:self
                          action:@selector(save:)
                forControlEvents:UIControlEventEditingDidEndOnExit];

    }
    
    //
    // MAIL
    //
    {
        __block UIImageView *invalidView = [[UIImageView alloc ] initWithImage:[UIImage imageNamed:@"invalid"]];
        {
            invalidView.contentMode = UIViewContentModeLeft; CGRect frame = invalidView.frame; frame.size.width += 2; [invalidView setFrame:frame];
        }
        __block UIImageView *validView = [[UIImageView alloc ] initWithImage:[UIImage imageNamed:@"valid"]];
        {
            validView.contentMode = UIViewContentModeLeft; CGRect frame = validView.frame; frame.size.width += 2; [validView setFrame:frame];
        }

        self.mail.rightViewMode = UITextFieldViewModeAlways;
        
        self.mail.floatingLabelFont = [UIFont boldSystemFontOfSize:9.5];
        [self.mail setPlaceholder:@"e-mail" floatingTitle:@"la tua e-mail (valida)"];
        
        __block NSString *emailPattern = @"^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
        
        [self.mail validationSetup:^BOOL(NSString *value) {
            return ([value rangeOfString:emailPattern options:NSRegularExpressionSearch].location != NSNotFound );
            
        } postValidation:^(BOOL valid) {
            __self.mail.rightView = (valid) ? validView : invalidView;
        }] ;

        [self.mail addTarget:self
                      action:@selector(save:)
            forControlEvents:UIControlEventEditingDidEndOnExit];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.username.text = [BSCUserPreferences sharedInstance].username; [self.username revalidate];
    self.mail.text = [BSCUserPreferences sharedInstance].mail; [self.mail revalidate];

    if (!self.username.valid) {
        [self.username becomeFirstResponder];
    } else if( !self.mail.valid ) {
        [self.mail becomeFirstResponder];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancel:(id)sender {

    __BLOCKSELF;
    
    if ([self.username isFirstResponder]) [self.username resignFirstResponder];
    if ([self.mail isFirstResponder]) [self.mail resignFirstResponder];
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (__self.onCancel ) {
            __self.onCancel();
        }
    }];
}

- (IBAction)save:(id)sender {

    if( !self.mail.valid || !self.username.valid ) return;
    
    __BLOCKSELF;
    
    if ([self.username isFirstResponder]) {
        [self.username resignFirstResponder];
    } else if ([self.mail isFirstResponder]) {
        [self.mail resignFirstResponder];
    }

    [BSCUserPreferences sharedInstance].username = self.username.text;
    [BSCUserPreferences sharedInstance].mail = self.mail.text;
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (__self.onSave ) {
            __self.onSave();
        }
    }];
}
@end
