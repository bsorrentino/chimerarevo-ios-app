//
//  CDVJSBridgePlugin.h
//  CordovaJSBridgeApp
//
//  Created by softphone on 18/01/14.
//
//

#import <Cordova/CDVPlugin.h>
#import <JavaScriptBridge/JavaScriptBridge.h>

@protocol JSBridgeExports <JSExport>

@property (nonatomic, weak) UIWebView* webView;
- (void)evalJs:(NSString *)js;
@end

@interface CDVJSBridge : CDVPlugin <JSBridgeExports>

- (void)loadModule:(CDVInvokedUrlCommand*)command;
- (void)exec:(CDVInvokedUrlCommand*)command;

@end
