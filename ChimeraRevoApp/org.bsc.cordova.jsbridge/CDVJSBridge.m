//
//  CDVJSBridgePlugin.m
//  CordovaJSBridgeApp
//
//  Created by softphone on 18/01/14.
//
//

#import "CDVJSBridge.h"
#import <JavaScriptBridge/JavaScriptBridge.h>

NSString * const CordovaJSBridgeException = @"JSBridgeException";
NSString * const CordovaJSBridgeEvaluationException = @"JSBridgeEvaluationException";

@interface CDVJSBridge ()


- (void)sendError:(CDVInvokedUrlCommand*)command msg:(NSString *)msg;
- (BOOL)checkException:(CDVInvokedUrlCommand*)command inContext:(JSContext *)ctx;
- (void)loadJSModule:(NSString*)moduleName inContext:(JSContext *)context; /*throws NSException*/

@end

@implementation CDVJSBridge


#pragma mark patch


#pragma mark private

- (void)sendError:(CDVInvokedUrlCommand*)command msg:(NSString *)msg
{
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:msg];
    [self.commandDelegate sendPluginResult:pluginResult
                                callbackId:command.callbackId];
}

- (BOOL)checkException:(CDVInvokedUrlCommand*)command inContext:(JSContext *)ctx
{
    if (ctx.exception == nil ) {
        return NO;
    }
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:ctx.exception.toString];
    [self.commandDelegate sendPluginResult:pluginResult
                                callbackId:command.callbackId];
    
    ctx.exception = nil;
    
    return YES;
}


#pragma mark lifecycle

- (void)pluginInitialize
{
    [super pluginInitialize];
    
    JSContext *context = [JSBScriptingSupport globalContext];
    
    context.exceptionHandler =
    ^(JSContext *context, JSValue *value) {
        
        NSLog(@"previous context.exception [%@]\n%@", context.exception, value.toString);
        
        context.exception = value;
    };
    
    context[@"JSBridge"] = [CDVJSBridge class];

    context[@"log"] = ^( NSString * msg ){
        
        NSLog(@"%@", msg);
    };
    

    context.exception = nil;
}

- (void)loadModule:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    if ([command.arguments count] == 0 ) {
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"miss mandatory argument"];
    }
    else {
        
        @try {
            
            NSString *name = [command argumentAtIndex:0];
            
            JSContext *context = [JSBScriptingSupport globalContext];
            
            [self loadJSModule:name inContext:context];
            
            pluginResult =
            [CDVPluginResult resultWithStatus:CDVCommandStatus_OK ];
           
        }
        @catch (NSException *exception) {
            
            NSDictionary* errDict = @{@"reason" : [exception reason] ,
                                      @"description": [exception description]};

            pluginResult =
            [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                              messageAsDictionary:errDict];
        }
        @finally {
            
        }
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)exec:(CDVInvokedUrlCommand*)command
{
    if ([command.arguments count] == 0 ) {
        
        [self sendError:command msg:@"miss mandatory argument"];
        return;
    }
    
    JSContext *context = [JSBScriptingSupport globalContext];
    
    NSString *functionName = [command argumentAtIndex:0];
    
    JSValue *function = context[functionName];
    
    if (function == nil || function.isNull || function.isUndefined ) {
        
        [self sendError:command
                    msg:[NSString stringWithFormat:@"function [%@] is missing!", functionName ]];
        return;
    }
    
    NSArray *arguments = [command arguments];
    
    NSArray *args = ([arguments count]==1) ?
                            @[] :
                            [arguments subarrayWithRange:NSMakeRange(1, [arguments count]-1 )];

    id currentSelf = context[@"self"];
    context[@"self"] = self;
    
    JSValue * result = [function callWithArguments:args];

    context[@"self"] = currentSelf;

    CDVPluginResult* pluginResult = nil;
    
    if( [self checkException:command inContext:context] ) {
        return;
    }
    
    if (result.isNull || result.isUndefined) {
        pluginResult =
        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK ];
    }
    else if( result.isNumber ) {
        pluginResult =
        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDouble:[result toDouble]];
    }
    else if( result.isBoolean) {
        pluginResult =
        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:[result toBool]];
    }
    else if( result.isString) {
        pluginResult =
        [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[result toString]];
    }
    else if( result.isObject) {
        
        if ([result isInstanceOf:[NSArray class]]) {
            pluginResult =
            [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:[result toArray] ];
        }
        else if ([result isInstanceOf:[NSDictionary class]]) {
            pluginResult =
            [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:[result toDictionary] ];
        }
        else if ([result isInstanceOf:[NSDate class]]) {
            pluginResult =
            [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"return type [Date] isn't supported yet!"];
            
        }
        else {
            
            NSString *type = NSStringFromClass([[result toObject] class]);
            
            pluginResult =
            [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                              messageAsString:[NSString stringWithFormat:@"return type [%@] isn't supported yet!", type ]];
        }
    }
        
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}


- (void)loadJSModule:(NSString*)moduleName inContext:(JSContext *)context
{
    if (moduleName==nil) {
        
        @throw [NSException exceptionWithName:NSInvalidArgumentException
                                       reason:@"moduleName is null"
                                     userInfo:nil];
    }
    if (context==nil) {
        
        @throw [NSException exceptionWithName:NSInvalidArgumentException
                                       reason:@"context is null"
                                     userInfo:nil];
    }
    
    NSString * resource1 = [NSString stringWithFormat:@"plugins/native/ios/%@.js", moduleName];
    
    NSString *path = [self.commandDelegate pathForResource:resource1];
    
    
    if (path == nil ) {
        
        NSString * resource2 = [NSString stringWithFormat:@"plugins/org.bsc.cordova.jsbridge/native/ios/%@.js", moduleName];
        
        path = [self.commandDelegate pathForResource:resource2];
        
        if (path == nil ) {
            @throw [NSException exceptionWithName:CordovaJSBridgeException
                                           reason:[NSString stringWithFormat:@"neither [%@] nor [%@] found!", resource1, resource2]
                                         userInfo:@{}];
        }
    }
    
    NSError *error = nil;
    
    
    NSString *script =
        [NSString stringWithContentsOfFile:path
                                  encoding:NSUTF8StringEncoding
                                     error:&error];
    if (error!=nil) {
        
         @throw [NSException exceptionWithName:CordovaJSBridgeException
                                 reason:[error description]
                               userInfo:[error userInfo]];
    }
    
    if( script != nil ) {
        
        [context evaluateScript:script];
        
        if (context.exception != nil ) {
            
            @throw [NSException exceptionWithName:CordovaJSBridgeEvaluationException
                                           reason:context.exception.toString
                                         userInfo:nil];
        }
    }
 
}

- (void)evalJs:(NSString *)js
{
    [self.commandDelegate evalJs:js];
}

@end
