//
//  BSCLoadPostViewController.m
//  ChimeraRevoApp
//
//  Created by softphone on 04/04/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import "BSCLoadPostViewController.h"

@interface BSCLoadPostViewController ()

@end

@implementation BSCLoadPostViewController

- (void)viewDidLoad
{
    
    //self.wwwFolderName = @"www/news";
    self.startPage = @"news.html";
    [super viewDidLoad];
    
    self.webView.opaque = NO;
    self.webView.backgroundColor = [UIColor clearColor];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Loading Animation 

- (void)startLoading
{
    [self.webView stringByEvaluatingJavaScriptFromString:@"window.startAnimation()"];
    
}

- (void)endLoading:(NSInteger)numOfNews
{

    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.endAnimation(%ld)", (long)numOfNews]];
    
}

@end
