//
//  BSCDetailViewController.h
//  ChimeraRevoApp
//
//  Created by softphone on 29/01/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSCDetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UITextView *contextText;


@end
