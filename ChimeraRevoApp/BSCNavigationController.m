//
//  BSCNavigationController.m
//  ChimeraRevoApp
//
//  Created by softphone on 11/06/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import "BSCNavigationController.h"

@interface BSCTitleView ()

@property (nonatomic,strong) UILabel *headerLabel;
@property (nonatomic,strong) UIImageView *logo;

- (CGRect)computeFrame:(NSString *)text theImage:(UIImage*)img theFont:(UIFont *)font ;

@end

const int labelGap = 3;

@implementation BSCTitleView

- (CGRect)computeFrame:(NSString *)text theImage:(UIImage*)img theFont:(UIFont *)font
{
    
    CGSize sz =  [text sizeWithAttributes:@{NSFontAttributeName: font}];
    
    CGRect frame = CGRectMake(0,0, img.size.width + labelGap + sz.width, img.size.height);
    
    return frame;
}

- (id)initWithTitle:(NSString *)value theFont:(UIFont *)font
{
    UIImage *img = [UIImage imageNamed:@"title"];

    CGRect frame = [self computeFrame:value theImage:img theFont:font];
    
    self = [super initWithFrame:frame];
    if (self) {
        
            CGSize sz = frame.size;
        
            _logo = [[UIImageView alloc] initWithImage:img];
            
            _logo.bounds = CGRectMake(0, 0, img.size.width, img.size.height);
            
            _headerLabel = [[UILabel alloc] initWithFrame:CGRectMake( img.size.width + labelGap , abs((img.size.height - sz.height))/2, sz.width, sz.height)];
            _headerLabel.text = value;
            _headerLabel.font = font;
            _headerLabel.backgroundColor = [UIColor clearColor];
            //_headerLabel.textColor = [UIColor whiteColor];
            //_headerLabel.shadowColor = [UIColor blackColor];
            //_headerLabel.shadowOffset = CGSizeMake(0, 1);
        
            [self addSubview:_logo];
            [self addSubview:_headerLabel];
            
        }
        return self;
}

- (NSString *)text
{
    return _headerLabel.text;
}

- (void) setText:(NSString *)value {
    
	if ([_headerLabel.text compare:value]==0) {
		return;
	}
  
    UIImage *img = _logo.image;
    
    CGRect frame = [self computeFrame:value theImage:img theFont:_headerLabel.font];

    CGSize sz =  frame.size;
    
	self.frame = frame;
	
	_headerLabel.frame =
        CGRectMake( img.size.width + labelGap , abs((img.size.height - sz.height))/2, sz.width, sz.height);
	
	_headerLabel.text = value;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end


