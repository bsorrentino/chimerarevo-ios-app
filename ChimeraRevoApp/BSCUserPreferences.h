//
//  BSCPreferences.h
//  ChimeraRevoApp
//
//  Created by Bartolomeo Sorrentino on 05/04/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <PAPreferences/PAPreferences.h>

@interface BSCUserPreferences : PAPreferences


@property (nonatomic, assign) NSString * lastPostDateAsString;
@property (nonatomic, assign) NSString * username;
@property (nonatomic, assign) NSString * mail;

- (NSDate *)lastPostDate;

@end
