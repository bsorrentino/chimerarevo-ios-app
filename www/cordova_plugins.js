cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/org.apache.cordova.console/www/console-via-logger.js",
        "id": "org.apache.cordova.console.console",
        "clobbers": [
            "console"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.console/www/logger.js",
        "id": "org.apache.cordova.console.logger",
        "clobbers": [
            "cordova.logger"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.inappbrowser/www/InAppBrowser.js",
        "id": "org.apache.cordova.inappbrowser.InAppBrowser",
        "clobbers": [
            "window.open"
        ]
    },
    {
        "file": "plugins/org.bsc.cordova.jsbridge/www/jsbridge.js",
        "id": "org.bsc.cordova.jsbridge.JSBridge",
        "clobbers": [
            "JSB"
        ]
    }

];
module.exports.metadata =
// TOP OF METADATA
{
    "org.apache.cordova.console": "0.2.7",
    "org.apache.cordova.inappbrowser": "0.3.1",
    "org.bsc.cordova.jsbridge": "1.0.0"

}
// BOTTOM OF METADATA
});
