

function initializeComment() {
    JSB.evaluate( "notification",
                 function(r) {
                    console.log("library loaded! "+r);
                 
                    JSB.method()
                        .result(function(r) { console.log("result of register is " + r); })
                        .error(function(e) { alert("error invoking register " + e);})
                        .exec("register");
                 
                    JSB.addHandler( "comment.added", function(n) {
                                
                        //alert("comment added! " + n.content);
                        appendComment( n.postId );
                                
                    });
                 
                 },
                 function(e) {
                    alert("error loading notification "+e);
                    console.log("error loading notification "+e);
                 }
        );
    
}

function reply( postId, commentId ) {
    
    JSB.method()
    .result(function(r) { console.log("result of reply is " + r); })
    .error(function(e) { alert("error invoking reply " + e);})
    .exec("reply", postId, commentId );
 
    //alert( "postId: " + postId + " reply to " + commentId );
    
}
/**
 * 
 * @param {type} id
 * @returns {void}
 */
function appendComment(id) {

    $('#comments_title').text('Commenti');
    $('#loading-indicator-comments').show();

    window.setTimeout(function() {

        var comments = $('.comments');

        comments.empty();

        $.getJSON(proxy.url("http://www.chimerarevo.com/api/1.0/posts/comments.json?count=100&postid=" + id), function(data) {

            $('#loading-indicator-comments').hide();

            $('#comments_title').text('Commenti - #' + data.comments.length);

            var root = parseTree(data.comments);

            navigateTree(root, 0, function(n, l) {

                var e = n.object;

                var style = (l <= 1) ? "initial" : "reply level" + (l - 1);

                $('<div class="' + style + '">' +
                        '<img src="images/avatar@48.png" alt="" />' +
                        '<a><img src="images/reply.png" alt="" onclick=reply("'+id+'","'+e.id+'") /></a>' +
                        '<span>' + e.author + '</span>' +
                        '<span>' + e.date + '</span>' +
                        '<p>' + e.content + '</p>' +
                        '</div>').appendTo(comments);

            });
        });

    }, 500);

}

  