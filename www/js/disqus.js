

function initializeComment() {

    JSB.evaluate( "notification",
             function(r) {
                console.log("library loaded! "+r);

                JSB.method()
                .result(function(r) { console.log("result of register is " + r); })
                .error(function(e) { alert("error invoking register " + e);})
                .exec("register");

                JSB.addHandler( "comment.open", function(n) {

                    var url = "./disqus.html?shortname=chimerarevo" +
                                    "&url="     + encodeURI(n.url) +
                                    "&title="   + encodeURI(n.title)
                                    ;
                    console.log("URL:" + url);
                    var w = window.open( url, "_blank", "location=no");

                    w.addEventListener("loadstop", function(e) {
                        // TEST DISQUS LOGIN
                        if(e.url.match(/^http:\/\/disqus\.com\/next\/login-success\//i)) {
                            // BACK DISQUS THREAD
                           w.executeScript( {code:"window.history.back();window.history.back();"}, null );
                        }
                        else if(e.url.match(/^http:\/\/disqus\.com\/_ax\/[\w]+\/complete\//i)) {
                            w.executeScript( {code:"window.history.back();window.history.back();window.history.back();"}, null );
                        }
                    });
                    w.addEventListener("loaderror", function(e) {
                           // TEST DISQUS LOGIN
                           w.executeScript( {code:"window.history.back();"}, null );

                    });

                },
                function(e) {
                    alert("error loading notification "+e);
                    console.log("error loading notification "+e);
                }
            );
    });
}

function getCommentCount( url ) {
    var disqusPublicKey = "gWfG2DJ6EbPrbo5DoO35vdH0IvqqcFtQQShetdYNJwsa0xxtzhqCTGdAi5ETQSok";
                               
    $.ajax({
      type: 'GET',
      url: "https://disqus.com/api/3.0/threads/set.jsonp",
      data: { api_key:disqusPublicKey, forum:"chimerarevo", thread:[ "link:" + url] }, // URL method
      cache: false,
      dataType: "jsonp",
      success: function (result) {
           var count = result.response[0].posts;
           console.log( "COMMENT# " + count);
           
           JSB.method()
           .result(function(r) { console.log("result of register is " + r); })
           .error(function(e) { alert("error invoking register " + e);})
           .exec("count", count);

           
      },
      error:function(result, status, error) {
           console.log( "error: " + status + " " + error.message );
           
           }
    });
                               
}
                               
function loadComments(shortname, url, title, identifier) {
    disqus_url = url;
    disqus_title = title;
    disqus_shortname = shortname;

    if (identifier != undefined)
        disqus_identifier = identifier;
    else
        disqus_identifier = "";

    (function() {
     var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = false;
     dsq.src = 'http://chimerarevo.disqus.com/embed.js';
     (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
     })();
}
