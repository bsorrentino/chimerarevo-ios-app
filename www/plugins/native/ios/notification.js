

var defaultCenter = NSNotificationCenter.defaultCenter();

function register() {
    
    JSB.dump(defaultCenter);
}

function count( v ) {
    
    JSB.log( "COUNT " + v );
    defaultCenter.postNotificationName( "comment.count", { "count":v } );
}

function reply( postId, replyId ) {
    
    JSB.log( "REPLY!" );
    defaultCenter.postNotificationName( "comment.reply", { "postId":postId, "replyId":replyId } );
}

