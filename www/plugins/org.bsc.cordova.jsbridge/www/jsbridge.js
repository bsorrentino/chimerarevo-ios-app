cordova.define("org.bsc.cordova.jsbridge.JSBridge", function(require, exports, module) { //cordova.define("org.bsc.cordova.jsbridge.JSBridge", function(require, exports, module) {


var exec = require('cordova/exec');

function _JSBridge() {
    this.returnCallback = null;
    this.errorCallback = null;
}

_JSBridge.prototype.result = function( callback ) {
    this.returnCallback = callback;
    return this;
}

_JSBridge.prototype.error = function( callback ) {
    
    this.errorCallback = callback
    return this;
}

_JSBridge.prototype.exec = function() {
    var params = [];
    
    for( var i = 0 ; i < arguments.length; ++i ) {
        params.push( arguments[i]);
    }
    
    exec(
         this.returnCallback,
         this.errorCallback,
         "JSBridge", "exec", params);
}


module.exports = {
    
    handlers:{},
    /**
     * Open a native alert dialog, with a customizable title and button text.
     *
     * @param {String}   moduleName
     * @param {Function} okCallback
     * @param {Function} errCallback
     */
    evaluate:function( moduleName, okCallback, errCallback) {
        exec(okCallback, errCallback, "JSBridge", "loadModule", [moduleName]);
    },
        
    method:function( /*callback*/ ) {
        return new _JSBridge();
    },
    
    addHandler:function( name, h) {
        this.handlers[name] = h;
    },
    
    invokeHandler:function( name, params ) {
        if( this.handlers[name] ) {
            var h = this.handlers[name];
            if( typeof h === 'function') { h( params );}
            else if( typeof h === 'string' ) { eval( h )( params ); }
        }
    }
    
};

// )};

});
