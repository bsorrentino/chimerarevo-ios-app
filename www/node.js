
function Node(object) {
    this.object = object;
    this.parent = null;
    this.children = [];
    
}
function parseTree( comments ) {
    
    var keys = {};
    
    var root = new Node(null);
    
    comments.reverse().forEach(function(c){
        var n = new Node(c);
        
        if( c.parent==='0') {           
            root.children.push( n );
            keys[c.id] = n;
        } else {
            var p = keys[c.parent];
            if( p ) { 
                n.parent = p;
                p.children.push( n );
                keys[c.id] = n;
            }
        }
    });
    
    root.children = root.children.reverse();
    return root;
}


function navigateTree( node, level, f ) {
    
    if( node.object ) {
        
        f( node, ++level );
    }
    
    if( node.children ) {
        
        node.children.forEach(function(c){

            navigateTree( c, level, f );
        });
    }
    
}
