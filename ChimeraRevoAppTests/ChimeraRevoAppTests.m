//
//  ChimeraRevoAppTests.m
//  ChimeraRevoAppTests
//
//  Created by softphone on 29/01/14.
//  Copyright (c) 2014 Soul software. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <MBRequest/MBRequest.h>


static inline void loadPosts( MBJSONRequestCompletionHandler handler )
{
    NSURL *url = [NSURL URLWithString:@"http://www.chimerarevo.com/api/1.0/posts/posts.json"];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    MBJSONRequest *jsonRequest = [[MBJSONRequest alloc] init];
    
    [jsonRequest performJSONRequest:urlRequest completionHandler:^(id responseJSON, NSError *error) {
        
        handler( responseJSON, error);
    }];
    
}


@interface ChimeraRevoAppTests : XCTestCase

@property (strong,nonatomic,readonly,getter = getDateFormatter) NSDateFormatter *dateFormatter;
@end



@implementation ChimeraRevoAppTests
@synthesize dateFormatter = _dateFormatter;

- (NSDateFormatter *)getDateFormatter
{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    }
    return _dateFormatter;
}
- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testDateFormat
{
    NSDate *date = [self.dateFormatter dateFromString:@"2014-03-09 09:00:31"];
    
    XCTAssertNotNil(date, @"date is nil!");
    
    NSDateComponents *dc =  [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitSecond|NSCalendarUnitMinute
                                                            fromDate:date];
    XCTAssertEqual( dc.year, 2014);
    XCTAssertEqual( dc.month, 3);
    XCTAssertEqual( dc.day, 9);
    XCTAssertEqual( dc.hour, 9);
    XCTAssertEqual( dc.minute, 0);
    XCTAssertEqual( dc.second, 31);
    
    NSLog( @"TMZ %@", [NSTimeZone knownTimeZoneNames] );

}

-(void)testDateDifference
{
    NSDate *date1 = [self.dateFormatter dateFromString:@"2014-03-09 09:00:31"];
    NSDate *date2 = [self.dateFormatter dateFromString:@"2014-03-01 00:00:00"];
    
    NSTimeInterval diff = [date1 timeIntervalSinceDate:date2];
    
    NSLog(@"time diff [%f]", diff);
    
    NSTimeInterval oneDay = 24 *3600;
    
    NSInteger numOfDay = diff / oneDay ;
    
    //XCTAssertEqual( diff, 8*oneDay, @"date diff doesn't mach!");
    XCTAssertEqual( numOfDay, 8, @"date diff doesn't mach!");
    
    
}

-(void)testOrderBy
{
    NSString *fileName = @"posts";
    
    NSString* path = [[NSBundle bundleForClass:[ChimeraRevoAppTests class]] pathForResource:fileName
                                                     ofType:@"json"];
    
    XCTAssertNotNil(path, @"file %@ not found!", fileName );
    
    NSError *error;
    
    NSData *content = [NSData dataWithContentsOfFile:path options:NSDataReadingUncached error:&error];
    
    XCTAssertNil(error, @"error reading content [%@]", [error description]);
    XCTAssertNotNil(content, @"content is nil!");
  
    id jsonData = [NSJSONSerialization JSONObjectWithData:content options:NSJSONReadingMutableContainers error:&error];

    XCTAssertNil(error, @"error converting content to json! [%@]", [error description]);
    XCTAssertNotNil(jsonData, @"jsondata is nil!");

    NSArray *posts = [jsonData valueForKey:@"posts"];
    
    XCTAssertNotNil(posts, @"json data doesn't contain 'posts!");
    XCTAssertTrue( [posts count] > 0, @"posts are empty!");

    /*
    NSPredicate *groupByDay = [NSPredicate predicateWithBlock: ^BOOL(id obj, NSDictionary *bind)
    {
        
        NSDate *date = [self.dateFormatter dateFromString:[obj valueForKey:@"data"] ];
        NSDateComponents *dc =  [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                                                fromDate:date];
        
        
        return YES;
    }];
    
    */
}


- (void)testMBRequest2
{
    __block BOOL loaded = NO;

    loadPosts(^(id responseJSON, NSError *error) {

        loaded = YES;
    });
    
    NSRunLoop *theRL = [NSRunLoop currentRunLoop];
    while (!loaded && [theRL runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]);
    
}

- (void)testMBRequest
{
    NSURL *url = [NSURL URLWithString:@"http://www.chimerarevo.com/api/1.0/posts/posts.json"];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    MBJSONRequest *jsonRequest = [[MBJSONRequest alloc] init];

    __block BOOL loaded = NO;
    
    [jsonRequest performJSONRequest:urlRequest completionHandler:^(id responseJSON, NSError *error) {
        
        XCTAssertNil(error, @"error on request: %@", error);
        
        NSString *title = [[[responseJSON objectForKey:@"posts"] objectAtIndex:0] objectForKey:@"title"];
        
        XCTAssertNotNil(title, @"title not found!");
        
        loaded = YES;
    }];
    
    NSRunLoop *theRL = [NSRunLoop currentRunLoop];
    while (!loaded && [theRL runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]);
}

@end
