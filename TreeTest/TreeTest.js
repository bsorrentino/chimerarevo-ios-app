
/////////////////////////////////////////////////////////

function navigateTree( node, level, f ) {
    
    if( node.object ) {
        
        f( node, level+1 );
    }
    
    if( node.children ) {
        
        node.children.forEach(function(c){

            navigateTree( c, level+1, f );
        });
    }
    
}

var http = require('http'),
        parseTree = require('./node');

var options = {
  host: 'www.chimerarevo.com',
  port: 80,
  path: '/api/1.0/posts/comments.json?postid=15&count=100'
};

http.get(options, function(res){

    var result = "";
    
    console.log( "statusCode ", res.statusCode );
    res.on('data', function(chunk){
        result += "" +chunk;
        //console.log( result );
        
    });
    res.on('end', function () {
        var comments = JSON.parse( result )['comments'];
        
        var root = parseTree( comments );

        console.log( '+', 'root' );        
        navigateTree( root, 1, function( node, level ) {
                var str = new Array(level + 1).join( '-' );
                console.log( str, node.object.id, node.object.date);

        });
    });    

}).on("error", function(e){
  console.log("Got error: " + e.message);
}).end();

